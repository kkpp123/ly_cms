Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root '/', controller: 'user', action: 'login'

  match 'login', to: 'user#login', via:[:get]

  resources :user do
    collection do
      match :login,via: [:get,:post]
      get :list
    end
  end
end
