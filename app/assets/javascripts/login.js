/*!
 * Bootstrap v3.3.6 (http://getbootstrap.com)
 * Copyright 2011-2015 Twitter, Inc.
 * Licensed under the MIT license
 */
if("undefined"==typeof jQuery)throw new Error("Bootstrap's JavaScript requires jQuery");+function(a){"use strict";var b=a.fn.jquery.split(" ")[0].split(".");if(b[0]<2&&b[1]<9||1==b[0]&&9==b[1]&&b[2]<1||b[0]>2)throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 3")}(jQuery),+function(a){"use strict";function b(){var a=document.createElement("bootstrap"),b={WebkitTransition:"webkitTransitionEnd",MozTransition:"transitionend",OTransition:"oTransitionEnd otransitionend",transition:"transitionend"};for(var c in b)if(void 0!==a.style[c])return{end:b[c]};return!1}a.fn.emulateTransitionEnd=function(b){var c=!1,d=this;a(this).one("bsTransitionEnd",function(){c=!0});var e=function(){c||a(d).trigger(a.support.transition.end)};return setTimeout(e,b),this},a(function(){a.support.transition=b(),a.support.transition&&(a.event.special.bsTransitionEnd={bindType:a.support.transition.end,delegateType:a.support.transition.end,handle:function(b){return a(b.target).is(this)?b.handleObj.handler.apply(this,arguments):void 0}})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var c=a(this),e=c.data("bs.alert");e||c.data("bs.alert",e=new d(this)),"string"==typeof b&&e[b].call(c)})}var c='[data-dismiss="alert"]',d=function(b){a(b).on("click",c,this.close)};d.VERSION="3.3.6",d.TRANSITION_DURATION=150,d.prototype.close=function(b){function c(){g.detach().trigger("closed.bs.alert").remove()}var e=a(this),f=e.attr("data-target");f||(f=e.attr("href"),f=f&&f.replace(/.*(?=#[^\s]*$)/,""));var g=a(f);b&&b.preventDefault(),g.length||(g=e.closest(".alert")),g.trigger(b=a.Event("close.bs.alert")),b.isDefaultPrevented()||(g.removeClass("in"),a.support.transition&&g.hasClass("fade")?g.one("bsTransitionEnd",c).emulateTransitionEnd(d.TRANSITION_DURATION):c())};var e=a.fn.alert;a.fn.alert=b,a.fn.alert.Constructor=d,a.fn.alert.noConflict=function(){return a.fn.alert=e,this},a(document).on("click.bs.alert.data-api",c,d.prototype.close)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.button"),f="object"==typeof b&&b;e||d.data("bs.button",e=new c(this,f)),"toggle"==b?e.toggle():b&&e.setState(b)})}var c=function(b,d){this.$element=a(b),this.options=a.extend({},c.DEFAULTS,d),this.isLoading=!1};c.VERSION="3.3.6",c.DEFAULTS={loadingText:"loading..."},c.prototype.setState=function(b){var c="disabled",d=this.$element,e=d.is("input")?"val":"html",f=d.data();b+="Text",null==f.resetText&&d.data("resetText",d[e]()),setTimeout(a.proxy(function(){d[e](null==f[b]?this.options[b]:f[b]),"loadingText"==b?(this.isLoading=!0,d.addClass(c).attr(c,c)):this.isLoading&&(this.isLoading=!1,d.removeClass(c).removeAttr(c))},this),0)},c.prototype.toggle=function(){var a=!0,b=this.$element.closest('[data-toggle="buttons"]');if(b.length){var c=this.$element.find("input");"radio"==c.prop("type")?(c.prop("checked")&&(a=!1),b.find(".active").removeClass("active"),this.$element.addClass("active")):"checkbox"==c.prop("type")&&(c.prop("checked")!==this.$element.hasClass("active")&&(a=!1),this.$element.toggleClass("active")),c.prop("checked",this.$element.hasClass("active")),a&&c.trigger("change")}else this.$element.attr("aria-pressed",!this.$element.hasClass("active")),this.$element.toggleClass("active")};var d=a.fn.button;a.fn.button=b,a.fn.button.Constructor=c,a.fn.button.noConflict=function(){return a.fn.button=d,this},a(document).on("click.bs.button.data-api",'[data-toggle^="button"]',function(c){var d=a(c.target);d.hasClass("btn")||(d=d.closest(".btn")),b.call(d,"toggle"),a(c.target).is('input[type="radio"]')||a(c.target).is('input[type="checkbox"]')||c.preventDefault()}).on("focus.bs.button.data-api blur.bs.button.data-api",'[data-toggle^="button"]',function(b){a(b.target).closest(".btn").toggleClass("focus",/^focus(in)?$/.test(b.type))})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.carousel"),f=a.extend({},c.DEFAULTS,d.data(),"object"==typeof b&&b),g="string"==typeof b?b:f.slide;e||d.data("bs.carousel",e=new c(this,f)),"number"==typeof b?e.to(b):g?e[g]():f.interval&&e.pause().cycle()})}var c=function(b,c){this.$element=a(b),this.$indicators=this.$element.find(".carousel-indicators"),this.options=c,this.paused=null,this.sliding=null,this.interval=null,this.$active=null,this.$items=null,this.options.keyboard&&this.$element.on("keydown.bs.carousel",a.proxy(this.keydown,this)),"hover"==this.options.pause&&!("ontouchstart"in document.documentElement)&&this.$element.on("mouseenter.bs.carousel",a.proxy(this.pause,this)).on("mouseleave.bs.carousel",a.proxy(this.cycle,this))};c.VERSION="3.3.6",c.TRANSITION_DURATION=600,c.DEFAULTS={interval:5e3,pause:"hover",wrap:!0,keyboard:!0},c.prototype.keydown=function(a){if(!/input|textarea/i.test(a.target.tagName)){switch(a.which){case 37:this.prev();break;case 39:this.next();break;default:return}a.preventDefault()}},c.prototype.cycle=function(b){return b||(this.paused=!1),this.interval&&clearInterval(this.interval),this.options.interval&&!this.paused&&(this.interval=setInterval(a.proxy(this.next,this),this.options.interval)),this},c.prototype.getItemIndex=function(a){return this.$items=a.parent().children(".item"),this.$items.index(a||this.$active)},c.prototype.getItemForDirection=function(a,b){var c=this.getItemIndex(b),d="prev"==a&&0===c||"next"==a&&c==this.$items.length-1;if(d&&!this.options.wrap)return b;var e="prev"==a?-1:1,f=(c+e)%this.$items.length;return this.$items.eq(f)},c.prototype.to=function(a){var b=this,c=this.getItemIndex(this.$active=this.$element.find(".item.active"));return a>this.$items.length-1||0>a?void 0:this.sliding?this.$element.one("slid.bs.carousel",function(){b.to(a)}):c==a?this.pause().cycle():this.slide(a>c?"next":"prev",this.$items.eq(a))},c.prototype.pause=function(b){return b||(this.paused=!0),this.$element.find(".next, .prev").length&&a.support.transition&&(this.$element.trigger(a.support.transition.end),this.cycle(!0)),this.interval=clearInterval(this.interval),this},c.prototype.next=function(){return this.sliding?void 0:this.slide("next")},c.prototype.prev=function(){return this.sliding?void 0:this.slide("prev")},c.prototype.slide=function(b,d){var e=this.$element.find(".item.active"),f=d||this.getItemForDirection(b,e),g=this.interval,h="next"==b?"left":"right",i=this;if(f.hasClass("active"))return this.sliding=!1;var j=f[0],k=a.Event("slide.bs.carousel",{relatedTarget:j,direction:h});if(this.$element.trigger(k),!k.isDefaultPrevented()){if(this.sliding=!0,g&&this.pause(),this.$indicators.length){this.$indicators.find(".active").removeClass("active");var l=a(this.$indicators.children()[this.getItemIndex(f)]);l&&l.addClass("active")}var m=a.Event("slid.bs.carousel",{relatedTarget:j,direction:h});return a.support.transition&&this.$element.hasClass("slide")?(f.addClass(b),f[0].offsetWidth,e.addClass(h),f.addClass(h),e.one("bsTransitionEnd",function(){f.removeClass([b,h].join(" ")).addClass("active"),e.removeClass(["active",h].join(" ")),i.sliding=!1,setTimeout(function(){i.$element.trigger(m)},0)}).emulateTransitionEnd(c.TRANSITION_DURATION)):(e.removeClass("active"),f.addClass("active"),this.sliding=!1,this.$element.trigger(m)),g&&this.cycle(),this}};var d=a.fn.carousel;a.fn.carousel=b,a.fn.carousel.Constructor=c,a.fn.carousel.noConflict=function(){return a.fn.carousel=d,this};var e=function(c){var d,e=a(this),f=a(e.attr("data-target")||(d=e.attr("href"))&&d.replace(/.*(?=#[^\s]+$)/,""));if(f.hasClass("carousel")){var g=a.extend({},f.data(),e.data()),h=e.attr("data-slide-to");h&&(g.interval=!1),b.call(f,g),h&&f.data("bs.carousel").to(h),c.preventDefault()}};a(document).on("click.bs.carousel.data-api","[data-slide]",e).on("click.bs.carousel.data-api","[data-slide-to]",e),a(window).on("load",function(){a('[data-ride="carousel"]').each(function(){var c=a(this);b.call(c,c.data())})})}(jQuery),+function(a){"use strict";function b(b){var c,d=b.attr("data-target")||(c=b.attr("href"))&&c.replace(/.*(?=#[^\s]+$)/,"");return a(d)}function c(b){return this.each(function(){var c=a(this),e=c.data("bs.collapse"),f=a.extend({},d.DEFAULTS,c.data(),"object"==typeof b&&b);!e&&f.toggle&&/show|hide/.test(b)&&(f.toggle=!1),e||c.data("bs.collapse",e=new d(this,f)),"string"==typeof b&&e[b]()})}var d=function(b,c){this.$element=a(b),this.options=a.extend({},d.DEFAULTS,c),this.$trigger=a('[data-toggle="collapse"][href="#'+b.id+'"],[data-toggle="collapse"][data-target="#'+b.id+'"]'),this.transitioning=null,this.options.parent?this.$parent=this.getParent():this.addAriaAndCollapsedClass(this.$element,this.$trigger),this.options.toggle&&this.toggle()};d.VERSION="3.3.6",d.TRANSITION_DURATION=350,d.DEFAULTS={toggle:!0},d.prototype.dimension=function(){var a=this.$element.hasClass("width");return a?"width":"height"},d.prototype.show=function(){if(!this.transitioning&&!this.$element.hasClass("in")){var b,e=this.$parent&&this.$parent.children(".panel").children(".in, .collapsing");if(!(e&&e.length&&(b=e.data("bs.collapse"),b&&b.transitioning))){var f=a.Event("show.bs.collapse");if(this.$element.trigger(f),!f.isDefaultPrevented()){e&&e.length&&(c.call(e,"hide"),b||e.data("bs.collapse",null));var g=this.dimension();this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded",!0),this.$trigger.removeClass("collapsed").attr("aria-expanded",!0),this.transitioning=1;var h=function(){this.$element.removeClass("collapsing").addClass("collapse in")[g](""),this.transitioning=0,this.$element.trigger("shown.bs.collapse")};if(!a.support.transition)return h.call(this);var i=a.camelCase(["scroll",g].join("-"));this.$element.one("bsTransitionEnd",a.proxy(h,this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i])}}}},d.prototype.hide=function(){if(!this.transitioning&&this.$element.hasClass("in")){var b=a.Event("hide.bs.collapse");if(this.$element.trigger(b),!b.isDefaultPrevented()){var c=this.dimension();this.$element[c](this.$element[c]())[0].offsetHeight,this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded",!1),this.$trigger.addClass("collapsed").attr("aria-expanded",!1),this.transitioning=1;var e=function(){this.transitioning=0,this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")};return a.support.transition?void this.$element[c](0).one("bsTransitionEnd",a.proxy(e,this)).emulateTransitionEnd(d.TRANSITION_DURATION):e.call(this)}}},d.prototype.toggle=function(){this[this.$element.hasClass("in")?"hide":"show"]()},d.prototype.getParent=function(){return a(this.options.parent).find('[data-toggle="collapse"][data-parent="'+this.options.parent+'"]').each(a.proxy(function(c,d){var e=a(d);this.addAriaAndCollapsedClass(b(e),e)},this)).end()},d.prototype.addAriaAndCollapsedClass=function(a,b){var c=a.hasClass("in");a.attr("aria-expanded",c),b.toggleClass("collapsed",!c).attr("aria-expanded",c)};var e=a.fn.collapse;a.fn.collapse=c,a.fn.collapse.Constructor=d,a.fn.collapse.noConflict=function(){return a.fn.collapse=e,this},a(document).on("click.bs.collapse.data-api",'[data-toggle="collapse"]',function(d){var e=a(this);e.attr("data-target")||d.preventDefault();var f=b(e),g=f.data("bs.collapse"),h=g?"toggle":e.data();c.call(f,h)})}(jQuery),+function(a){"use strict";function b(b){var c=b.attr("data-target");c||(c=b.attr("href"),c=c&&/#[A-Za-z]/.test(c)&&c.replace(/.*(?=#[^\s]*$)/,""));var d=c&&a(c);return d&&d.length?d:b.parent()}function c(c){c&&3===c.which||(a(e).remove(),a(f).each(function(){var d=a(this),e=b(d),f={relatedTarget:this};e.hasClass("open")&&(c&&"click"==c.type&&/input|textarea/i.test(c.target.tagName)&&a.contains(e[0],c.target)||(e.trigger(c=a.Event("hide.bs.dropdown",f)),c.isDefaultPrevented()||(d.attr("aria-expanded","false"),e.removeClass("open").trigger(a.Event("hidden.bs.dropdown",f)))))}))}function d(b){return this.each(function(){var c=a(this),d=c.data("bs.dropdown");d||c.data("bs.dropdown",d=new g(this)),"string"==typeof b&&d[b].call(c)})}var e=".dropdown-backdrop",f='[data-toggle="dropdown"]',g=function(b){a(b).on("click.bs.dropdown",this.toggle)};g.VERSION="3.3.6",g.prototype.toggle=function(d){var e=a(this);if(!e.is(".disabled, :disabled")){var f=b(e),g=f.hasClass("open");if(c(),!g){"ontouchstart"in document.documentElement&&!f.closest(".navbar-nav").length&&a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click",c);var h={relatedTarget:this};if(f.trigger(d=a.Event("show.bs.dropdown",h)),d.isDefaultPrevented())return;e.trigger("focus").attr("aria-expanded","true"),f.toggleClass("open").trigger(a.Event("shown.bs.dropdown",h))}return!1}},g.prototype.keydown=function(c){if(/(38|40|27|32)/.test(c.which)&&!/input|textarea/i.test(c.target.tagName)){var d=a(this);if(c.preventDefault(),c.stopPropagation(),!d.is(".disabled, :disabled")){var e=b(d),g=e.hasClass("open");if(!g&&27!=c.which||g&&27==c.which)return 27==c.which&&e.find(f).trigger("focus"),d.trigger("click");var h=" li:not(.disabled):visible a",i=e.find(".dropdown-menu"+h);if(i.length){var j=i.index(c.target);38==c.which&&j>0&&j--,40==c.which&&j<i.length-1&&j++,~j||(j=0),i.eq(j).trigger("focus")}}}};var h=a.fn.dropdown;a.fn.dropdown=d,a.fn.dropdown.Constructor=g,a.fn.dropdown.noConflict=function(){return a.fn.dropdown=h,this},a(document).on("click.bs.dropdown.data-api",c).on("click.bs.dropdown.data-api",".dropdown form",function(a){a.stopPropagation()}).on("click.bs.dropdown.data-api",f,g.prototype.toggle).on("keydown.bs.dropdown.data-api",f,g.prototype.keydown).on("keydown.bs.dropdown.data-api",".dropdown-menu",g.prototype.keydown)}(jQuery),+function(a){"use strict";function b(b,d){return this.each(function(){var e=a(this),f=e.data("bs.modal"),g=a.extend({},c.DEFAULTS,e.data(),"object"==typeof b&&b);f||e.data("bs.modal",f=new c(this,g)),"string"==typeof b?f[b](d):g.show&&f.show(d)})}var c=function(b,c){this.options=c,this.$body=a(document.body),this.$element=a(b),this.$dialog=this.$element.find(".modal-dialog"),this.$backdrop=null,this.isShown=null,this.originalBodyPad=null,this.scrollbarWidth=0,this.ignoreBackdropClick=!1,this.options.remote&&this.$element.find(".modal-content").load(this.options.remote,a.proxy(function(){this.$element.trigger("loaded.bs.modal")},this))};c.VERSION="3.3.6",c.TRANSITION_DURATION=300,c.BACKDROP_TRANSITION_DURATION=150,c.DEFAULTS={backdrop:!0,keyboard:!0,show:!0},c.prototype.toggle=function(a){return this.isShown?this.hide():this.show(a)},c.prototype.show=function(b){var d=this,e=a.Event("show.bs.modal",{relatedTarget:b});this.$element.trigger(e),this.isShown||e.isDefaultPrevented()||(this.isShown=!0,this.checkScrollbar(),this.setScrollbar(),this.$body.addClass("modal-open"),this.escape(),this.resize(),this.$element.on("click.dismiss.bs.modal",'[data-dismiss="modal"]',a.proxy(this.hide,this)),this.$dialog.on("mousedown.dismiss.bs.modal",function(){d.$element.one("mouseup.dismiss.bs.modal",function(b){a(b.target).is(d.$element)&&(d.ignoreBackdropClick=!0)})}),this.backdrop(function(){var e=a.support.transition&&d.$element.hasClass("fade");d.$element.parent().length||d.$element.appendTo(d.$body),d.$element.show().scrollTop(0),d.adjustDialog(),e&&d.$element[0].offsetWidth,d.$element.addClass("in"),d.enforceFocus();var f=a.Event("shown.bs.modal",{relatedTarget:b});e?d.$dialog.one("bsTransitionEnd",function(){d.$element.trigger("focus").trigger(f)}).emulateTransitionEnd(c.TRANSITION_DURATION):d.$element.trigger("focus").trigger(f)}))},c.prototype.hide=function(b){b&&b.preventDefault(),b=a.Event("hide.bs.modal"),this.$element.trigger(b),this.isShown&&!b.isDefaultPrevented()&&(this.isShown=!1,this.escape(),this.resize(),a(document).off("focusin.bs.modal"),this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),this.$dialog.off("mousedown.dismiss.bs.modal"),a.support.transition&&this.$element.hasClass("fade")?this.$element.one("bsTransitionEnd",a.proxy(this.hideModal,this)).emulateTransitionEnd(c.TRANSITION_DURATION):this.hideModal())},c.prototype.enforceFocus=function(){a(document).off("focusin.bs.modal").on("focusin.bs.modal",a.proxy(function(a){this.$element[0]===a.target||this.$element.has(a.target).length||this.$element.trigger("focus")},this))},c.prototype.escape=function(){this.isShown&&this.options.keyboard?this.$element.on("keydown.dismiss.bs.modal",a.proxy(function(a){27==a.which&&this.hide()},this)):this.isShown||this.$element.off("keydown.dismiss.bs.modal")},c.prototype.resize=function(){this.isShown?a(window).on("resize.bs.modal",a.proxy(this.handleUpdate,this)):a(window).off("resize.bs.modal")},c.prototype.hideModal=function(){var a=this;this.$element.hide(),this.backdrop(function(){a.$body.removeClass("modal-open"),a.resetAdjustments(),a.resetScrollbar(),a.$element.trigger("hidden.bs.modal")})},c.prototype.removeBackdrop=function(){this.$backdrop&&this.$backdrop.remove(),this.$backdrop=null},c.prototype.backdrop=function(b){var d=this,e=this.$element.hasClass("fade")?"fade":"";if(this.isShown&&this.options.backdrop){var f=a.support.transition&&e;if(this.$backdrop=a(document.createElement("div")).addClass("modal-backdrop "+e).appendTo(this.$body),this.$element.on("click.dismiss.bs.modal",a.proxy(function(a){return this.ignoreBackdropClick?void(this.ignoreBackdropClick=!1):void(a.target===a.currentTarget&&("static"==this.options.backdrop?this.$element[0].focus():this.hide()))},this)),f&&this.$backdrop[0].offsetWidth,this.$backdrop.addClass("in"),!b)return;f?this.$backdrop.one("bsTransitionEnd",b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):b()}else if(!this.isShown&&this.$backdrop){this.$backdrop.removeClass("in");var g=function(){d.removeBackdrop(),b&&b()};a.support.transition&&this.$element.hasClass("fade")?this.$backdrop.one("bsTransitionEnd",g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION):g()}else b&&b()},c.prototype.handleUpdate=function(){this.adjustDialog()},c.prototype.adjustDialog=function(){var a=this.$element[0].scrollHeight>document.documentElement.clientHeight;this.$element.css({paddingLeft:!this.bodyIsOverflowing&&a?this.scrollbarWidth:"",paddingRight:this.bodyIsOverflowing&&!a?this.scrollbarWidth:""})},c.prototype.resetAdjustments=function(){this.$element.css({paddingLeft:"",paddingRight:""})},c.prototype.checkScrollbar=function(){var a=window.innerWidth;if(!a){var b=document.documentElement.getBoundingClientRect();a=b.right-Math.abs(b.left)}this.bodyIsOverflowing=document.body.clientWidth<a,this.scrollbarWidth=this.measureScrollbar()},c.prototype.setScrollbar=function(){var a=parseInt(this.$body.css("padding-right")||0,10);this.originalBodyPad=document.body.style.paddingRight||"",this.bodyIsOverflowing&&this.$body.css("padding-right",a+this.scrollbarWidth)},c.prototype.resetScrollbar=function(){this.$body.css("padding-right",this.originalBodyPad)},c.prototype.measureScrollbar=function(){var a=document.createElement("div");a.className="modal-scrollbar-measure",this.$body.append(a);var b=a.offsetWidth-a.clientWidth;return this.$body[0].removeChild(a),b};var d=a.fn.modal;a.fn.modal=b,a.fn.modal.Constructor=c,a.fn.modal.noConflict=function(){return a.fn.modal=d,this},a(document).on("click.bs.modal.data-api",'[data-toggle="modal"]',function(c){var d=a(this),e=d.attr("href"),f=a(d.attr("data-target")||e&&e.replace(/.*(?=#[^\s]+$)/,"")),g=f.data("bs.modal")?"toggle":a.extend({remote:!/#/.test(e)&&e},f.data(),d.data());d.is("a")&&c.preventDefault(),f.one("show.bs.modal",function(a){a.isDefaultPrevented()||f.one("hidden.bs.modal",function(){d.is(":visible")&&d.trigger("focus")})}),b.call(f,g,this)})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tooltip"),f="object"==typeof b&&b;(e||!/destroy|hide/.test(b))&&(e||d.data("bs.tooltip",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.type=null,this.options=null,this.enabled=null,this.timeout=null,this.hoverState=null,this.$element=null,this.inState=null,this.init("tooltip",a,b)};c.VERSION="3.3.6",c.TRANSITION_DURATION=150,c.DEFAULTS={animation:!0,placement:"top",selector:!1,template:'<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',trigger:"hover focus",title:"",delay:0,html:!1,container:!1,viewport:{selector:"body",padding:0}},c.prototype.init=function(b,c,d){if(this.enabled=!0,this.type=b,this.$element=a(c),this.options=this.getOptions(d),this.$viewport=this.options.viewport&&a(a.isFunction(this.options.viewport)?this.options.viewport.call(this,this.$element):this.options.viewport.selector||this.options.viewport),this.inState={click:!1,hover:!1,focus:!1},this.$element[0]instanceof document.constructor&&!this.options.selector)throw new Error("`selector` option must be specified when initializing "+this.type+" on the window.document object!");for(var e=this.options.trigger.split(" "),f=e.length;f--;){var g=e[f];if("click"==g)this.$element.on("click."+this.type,this.options.selector,a.proxy(this.toggle,this));else if("manual"!=g){var h="hover"==g?"mouseenter":"focusin",i="hover"==g?"mouseleave":"focusout";this.$element.on(h+"."+this.type,this.options.selector,a.proxy(this.enter,this)),this.$element.on(i+"."+this.type,this.options.selector,a.proxy(this.leave,this))}}this.options.selector?this._options=a.extend({},this.options,{trigger:"manual",selector:""}):this.fixTitle()},c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.getOptions=function(b){return b=a.extend({},this.getDefaults(),this.$element.data(),b),b.delay&&"number"==typeof b.delay&&(b.delay={show:b.delay,hide:b.delay}),b},c.prototype.getDelegateOptions=function(){var b={},c=this.getDefaults();return this._options&&a.each(this._options,function(a,d){c[a]!=d&&(b[a]=d)}),b},c.prototype.enter=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);return c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),b instanceof a.Event&&(c.inState["focusin"==b.type?"focus":"hover"]=!0),c.tip().hasClass("in")||"in"==c.hoverState?void(c.hoverState="in"):(clearTimeout(c.timeout),c.hoverState="in",c.options.delay&&c.options.delay.show?void(c.timeout=setTimeout(function(){"in"==c.hoverState&&c.show()},c.options.delay.show)):c.show())},c.prototype.isInStateTrue=function(){for(var a in this.inState)if(this.inState[a])return!0;return!1},c.prototype.leave=function(b){var c=b instanceof this.constructor?b:a(b.currentTarget).data("bs."+this.type);return c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c)),b instanceof a.Event&&(c.inState["focusout"==b.type?"focus":"hover"]=!1),c.isInStateTrue()?void 0:(clearTimeout(c.timeout),c.hoverState="out",c.options.delay&&c.options.delay.hide?void(c.timeout=setTimeout(function(){"out"==c.hoverState&&c.hide()},c.options.delay.hide)):c.hide())},c.prototype.show=function(){var b=a.Event("show.bs."+this.type);if(this.hasContent()&&this.enabled){this.$element.trigger(b);var d=a.contains(this.$element[0].ownerDocument.documentElement,this.$element[0]);if(b.isDefaultPrevented()||!d)return;var e=this,f=this.tip(),g=this.getUID(this.type);this.setContent(),f.attr("id",g),this.$element.attr("aria-describedby",g),this.options.animation&&f.addClass("fade");var h="function"==typeof this.options.placement?this.options.placement.call(this,f[0],this.$element[0]):this.options.placement,i=/\s?auto?\s?/i,j=i.test(h);j&&(h=h.replace(i,"")||"top"),f.detach().css({top:0,left:0,display:"block"}).addClass(h).data("bs."+this.type,this),this.options.container?f.appendTo(this.options.container):f.insertAfter(this.$element),this.$element.trigger("inserted.bs."+this.type);var k=this.getPosition(),l=f[0].offsetWidth,m=f[0].offsetHeight;if(j){var n=h,o=this.getPosition(this.$viewport);h="bottom"==h&&k.bottom+m>o.bottom?"top":"top"==h&&k.top-m<o.top?"bottom":"right"==h&&k.right+l>o.width?"left":"left"==h&&k.left-l<o.left?"right":h,f.removeClass(n).addClass(h)}var p=this.getCalculatedOffset(h,k,l,m);this.applyPlacement(p,h);var q=function(){var a=e.hoverState;e.$element.trigger("shown.bs."+e.type),e.hoverState=null,"out"==a&&e.leave(e)};a.support.transition&&this.$tip.hasClass("fade")?f.one("bsTransitionEnd",q).emulateTransitionEnd(c.TRANSITION_DURATION):q()}},c.prototype.applyPlacement=function(b,c){var d=this.tip(),e=d[0].offsetWidth,f=d[0].offsetHeight,g=parseInt(d.css("margin-top"),10),h=parseInt(d.css("margin-left"),10);isNaN(g)&&(g=0),isNaN(h)&&(h=0),b.top+=g,b.left+=h,a.offset.setOffset(d[0],a.extend({using:function(a){d.css({top:Math.round(a.top),left:Math.round(a.left)})}},b),0),d.addClass("in");var i=d[0].offsetWidth,j=d[0].offsetHeight;"top"==c&&j!=f&&(b.top=b.top+f-j);var k=this.getViewportAdjustedDelta(c,b,i,j);k.left?b.left+=k.left:b.top+=k.top;var l=/top|bottom/.test(c),m=l?2*k.left-e+i:2*k.top-f+j,n=l?"offsetWidth":"offsetHeight";d.offset(b),this.replaceArrow(m,d[0][n],l)},c.prototype.replaceArrow=function(a,b,c){this.arrow().css(c?"left":"top",50*(1-a/b)+"%").css(c?"top":"left","")},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle();a.find(".tooltip-inner")[this.options.html?"html":"text"](b),a.removeClass("fade in top bottom left right")},c.prototype.hide=function(b){function d(){"in"!=e.hoverState&&f.detach(),e.$element.removeAttr("aria-describedby").trigger("hidden.bs."+e.type),b&&b()}var e=this,f=a(this.$tip),g=a.Event("hide.bs."+this.type);return this.$element.trigger(g),g.isDefaultPrevented()?void 0:(f.removeClass("in"),a.support.transition&&f.hasClass("fade")?f.one("bsTransitionEnd",d).emulateTransitionEnd(c.TRANSITION_DURATION):d(),this.hoverState=null,this)},c.prototype.fixTitle=function(){var a=this.$element;(a.attr("title")||"string"!=typeof a.attr("data-original-title"))&&a.attr("data-original-title",a.attr("title")||"").attr("title","")},c.prototype.hasContent=function(){return this.getTitle()},c.prototype.getPosition=function(b){b=b||this.$element;var c=b[0],d="BODY"==c.tagName,e=c.getBoundingClientRect();null==e.width&&(e=a.extend({},e,{width:e.right-e.left,height:e.bottom-e.top}));var f=d?{top:0,left:0}:b.offset(),g={scroll:d?document.documentElement.scrollTop||document.body.scrollTop:b.scrollTop()},h=d?{width:a(window).width(),height:a(window).height()}:null;return a.extend({},e,g,h,f)},c.prototype.getCalculatedOffset=function(a,b,c,d){return"bottom"==a?{top:b.top+b.height,left:b.left+b.width/2-c/2}:"top"==a?{top:b.top-d,left:b.left+b.width/2-c/2}:"left"==a?{top:b.top+b.height/2-d/2,left:b.left-c}:{top:b.top+b.height/2-d/2,left:b.left+b.width}},c.prototype.getViewportAdjustedDelta=function(a,b,c,d){var e={top:0,left:0};if(!this.$viewport)return e;var f=this.options.viewport&&this.options.viewport.padding||0,g=this.getPosition(this.$viewport);if(/right|left/.test(a)){var h=b.top-f-g.scroll,i=b.top+f-g.scroll+d;h<g.top?e.top=g.top-h:i>g.top+g.height&&(e.top=g.top+g.height-i)}else{var j=b.left-f,k=b.left+f+c;j<g.left?e.left=g.left-j:k>g.right&&(e.left=g.left+g.width-k)}return e},c.prototype.getTitle=function(){var a,b=this.$element,c=this.options;return a=b.attr("data-original-title")||("function"==typeof c.title?c.title.call(b[0]):c.title)},c.prototype.getUID=function(a){do a+=~~(1e6*Math.random());while(document.getElementById(a));return a},c.prototype.tip=function(){if(!this.$tip&&(this.$tip=a(this.options.template),1!=this.$tip.length))throw new Error(this.type+" `template` option must consist of exactly 1 top-level element!");return this.$tip},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".tooltip-arrow")},c.prototype.enable=function(){this.enabled=!0},c.prototype.disable=function(){this.enabled=!1},c.prototype.toggleEnabled=function(){this.enabled=!this.enabled},c.prototype.toggle=function(b){var c=this;b&&(c=a(b.currentTarget).data("bs."+this.type),c||(c=new this.constructor(b.currentTarget,this.getDelegateOptions()),a(b.currentTarget).data("bs."+this.type,c))),b?(c.inState.click=!c.inState.click,c.isInStateTrue()?c.enter(c):c.leave(c)):c.tip().hasClass("in")?c.leave(c):c.enter(c)},c.prototype.destroy=function(){var a=this;clearTimeout(this.timeout),this.hide(function(){a.$element.off("."+a.type).removeData("bs."+a.type),a.$tip&&a.$tip.detach(),a.$tip=null,a.$arrow=null,a.$viewport=null})};var d=a.fn.tooltip;a.fn.tooltip=b,a.fn.tooltip.Constructor=c,a.fn.tooltip.noConflict=function(){return a.fn.tooltip=d,this}}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.popover"),f="object"==typeof b&&b;(e||!/destroy|hide/.test(b))&&(e||d.data("bs.popover",e=new c(this,f)),"string"==typeof b&&e[b]())})}var c=function(a,b){this.init("popover",a,b)};if(!a.fn.tooltip)throw new Error("Popover requires tooltip.js");c.VERSION="3.3.6",c.DEFAULTS=a.extend({},a.fn.tooltip.Constructor.DEFAULTS,{placement:"right",trigger:"click",content:"",template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'}),c.prototype=a.extend({},a.fn.tooltip.Constructor.prototype),c.prototype.constructor=c,c.prototype.getDefaults=function(){return c.DEFAULTS},c.prototype.setContent=function(){var a=this.tip(),b=this.getTitle(),c=this.getContent();a.find(".popover-title")[this.options.html?"html":"text"](b),a.find(".popover-content").children().detach().end()[this.options.html?"string"==typeof c?"html":"append":"text"](c),a.removeClass("fade top bottom left right in"),a.find(".popover-title").html()||a.find(".popover-title").hide()},c.prototype.hasContent=function(){return this.getTitle()||this.getContent()},c.prototype.getContent=function(){var a=this.$element,b=this.options;return a.attr("data-content")||("function"==typeof b.content?b.content.call(a[0]):b.content)},c.prototype.arrow=function(){return this.$arrow=this.$arrow||this.tip().find(".arrow")};var d=a.fn.popover;a.fn.popover=b,a.fn.popover.Constructor=c,a.fn.popover.noConflict=function(){return a.fn.popover=d,this}}(jQuery),+function(a){"use strict";function b(c,d){this.$body=a(document.body),this.$scrollElement=a(a(c).is(document.body)?window:c),this.options=a.extend({},b.DEFAULTS,d),this.selector=(this.options.target||"")+" .nav li > a",this.offsets=[],this.targets=[],this.activeTarget=null,this.scrollHeight=0,this.$scrollElement.on("scroll.bs.scrollspy",a.proxy(this.process,this)),this.refresh(),this.process()}function c(c){return this.each(function(){var d=a(this),e=d.data("bs.scrollspy"),f="object"==typeof c&&c;e||d.data("bs.scrollspy",e=new b(this,f)),"string"==typeof c&&e[c]()})}b.VERSION="3.3.6",b.DEFAULTS={offset:10},b.prototype.getScrollHeight=function(){return this.$scrollElement[0].scrollHeight||Math.max(this.$body[0].scrollHeight,document.documentElement.scrollHeight)},b.prototype.refresh=function(){var b=this,c="offset",d=0;this.offsets=[],this.targets=[],this.scrollHeight=this.getScrollHeight(),a.isWindow(this.$scrollElement[0])||(c="position",d=this.$scrollElement.scrollTop()),this.$body.find(this.selector).map(function(){var b=a(this),e=b.data("target")||b.attr("href"),f=/^#./.test(e)&&a(e);return f&&f.length&&f.is(":visible")&&[[f[c]().top+d,e]]||null}).sort(function(a,b){return a[0]-b[0]}).each(function(){b.offsets.push(this[0]),b.targets.push(this[1])})},b.prototype.process=function(){var a,b=this.$scrollElement.scrollTop()+this.options.offset,c=this.getScrollHeight(),d=this.options.offset+c-this.$scrollElement.height(),e=this.offsets,f=this.targets,g=this.activeTarget;if(this.scrollHeight!=c&&this.refresh(),b>=d)return g!=(a=f[f.length-1])&&this.activate(a);if(g&&b<e[0])return this.activeTarget=null,this.clear();for(a=e.length;a--;)g!=f[a]&&b>=e[a]&&(void 0===e[a+1]||b<e[a+1])&&this.activate(f[a])},b.prototype.activate=function(b){this.activeTarget=b,this.clear();var c=this.selector+'[data-target="'+b+'"],'+this.selector+'[href="'+b+'"]',d=a(c).parents("li").addClass("active");
d.parent(".dropdown-menu").length&&(d=d.closest("li.dropdown").addClass("active")),d.trigger("activate.bs.scrollspy")},b.prototype.clear=function(){a(this.selector).parentsUntil(this.options.target,".active").removeClass("active")};var d=a.fn.scrollspy;a.fn.scrollspy=c,a.fn.scrollspy.Constructor=b,a.fn.scrollspy.noConflict=function(){return a.fn.scrollspy=d,this},a(window).on("load.bs.scrollspy.data-api",function(){a('[data-spy="scroll"]').each(function(){var b=a(this);c.call(b,b.data())})})}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.tab");e||d.data("bs.tab",e=new c(this)),"string"==typeof b&&e[b]()})}var c=function(b){this.element=a(b)};c.VERSION="3.3.6",c.TRANSITION_DURATION=150,c.prototype.show=function(){var b=this.element,c=b.closest("ul:not(.dropdown-menu)"),d=b.data("target");if(d||(d=b.attr("href"),d=d&&d.replace(/.*(?=#[^\s]*$)/,"")),!b.parent("li").hasClass("active")){var e=c.find(".active:last a"),f=a.Event("hide.bs.tab",{relatedTarget:b[0]}),g=a.Event("show.bs.tab",{relatedTarget:e[0]});if(e.trigger(f),b.trigger(g),!g.isDefaultPrevented()&&!f.isDefaultPrevented()){var h=a(d);this.activate(b.closest("li"),c),this.activate(h,h.parent(),function(){e.trigger({type:"hidden.bs.tab",relatedTarget:b[0]}),b.trigger({type:"shown.bs.tab",relatedTarget:e[0]})})}}},c.prototype.activate=function(b,d,e){function f(){g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!1),b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded",!0),h?(b[0].offsetWidth,b.addClass("in")):b.removeClass("fade"),b.parent(".dropdown-menu").length&&b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded",!0),e&&e()}var g=d.find("> .active"),h=e&&a.support.transition&&(g.length&&g.hasClass("fade")||!!d.find("> .fade").length);g.length&&h?g.one("bsTransitionEnd",f).emulateTransitionEnd(c.TRANSITION_DURATION):f(),g.removeClass("in")};var d=a.fn.tab;a.fn.tab=b,a.fn.tab.Constructor=c,a.fn.tab.noConflict=function(){return a.fn.tab=d,this};var e=function(c){c.preventDefault(),b.call(a(this),"show")};a(document).on("click.bs.tab.data-api",'[data-toggle="tab"]',e).on("click.bs.tab.data-api",'[data-toggle="pill"]',e)}(jQuery),+function(a){"use strict";function b(b){return this.each(function(){var d=a(this),e=d.data("bs.affix"),f="object"==typeof b&&b;e||d.data("bs.affix",e=new c(this,f)),"string"==typeof b&&e[b]()})}var c=function(b,d){this.options=a.extend({},c.DEFAULTS,d),this.$target=a(this.options.target).on("scroll.bs.affix.data-api",a.proxy(this.checkPosition,this)).on("click.bs.affix.data-api",a.proxy(this.checkPositionWithEventLoop,this)),this.$element=a(b),this.affixed=null,this.unpin=null,this.pinnedOffset=null,this.checkPosition()};c.VERSION="3.3.6",c.RESET="affix affix-top affix-bottom",c.DEFAULTS={offset:0,target:window},c.prototype.getState=function(a,b,c,d){var e=this.$target.scrollTop(),f=this.$element.offset(),g=this.$target.height();if(null!=c&&"top"==this.affixed)return c>e?"top":!1;if("bottom"==this.affixed)return null!=c?e+this.unpin<=f.top?!1:"bottom":a-d>=e+g?!1:"bottom";var h=null==this.affixed,i=h?e:f.top,j=h?g:b;return null!=c&&c>=e?"top":null!=d&&i+j>=a-d?"bottom":!1},c.prototype.getPinnedOffset=function(){if(this.pinnedOffset)return this.pinnedOffset;this.$element.removeClass(c.RESET).addClass("affix");var a=this.$target.scrollTop(),b=this.$element.offset();return this.pinnedOffset=b.top-a},c.prototype.checkPositionWithEventLoop=function(){setTimeout(a.proxy(this.checkPosition,this),1)},c.prototype.checkPosition=function(){if(this.$element.is(":visible")){var b=this.$element.height(),d=this.options.offset,e=d.top,f=d.bottom,g=Math.max(a(document).height(),a(document.body).height());"object"!=typeof d&&(f=e=d),"function"==typeof e&&(e=d.top(this.$element)),"function"==typeof f&&(f=d.bottom(this.$element));var h=this.getState(g,b,e,f);if(this.affixed!=h){null!=this.unpin&&this.$element.css("top","");var i="affix"+(h?"-"+h:""),j=a.Event(i+".bs.affix");if(this.$element.trigger(j),j.isDefaultPrevented())return;this.affixed=h,this.unpin="bottom"==h?this.getPinnedOffset():null,this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix","affixed")+".bs.affix")}"bottom"==h&&this.$element.offset({top:g-b-f})}};var d=a.fn.affix;a.fn.affix=b,a.fn.affix.Constructor=c,a.fn.affix.noConflict=function(){return a.fn.affix=d,this},a(window).on("load",function(){a('[data-spy="affix"]').each(function(){var c=a(this),d=c.data();d.offset=d.offset||{},null!=d.offsetBottom&&(d.offset.bottom=d.offsetBottom),null!=d.offsetTop&&(d.offset.top=d.offsetTop),b.call(c,d)})})}(jQuery);
/*!
 * iCheck v0.9.1 jQuery plugin, http://git.io/uhUPMA
 */
(function(f){function C(a,c,d){var b=a[0],e=/er/.test(d)?k:/bl/.test(d)?u:j;active=d==E?{checked:b[j],disabled:b[u],indeterminate:"true"==a.attr(k)||"false"==a.attr(v)}:b[e];if(/^(ch|di|in)/.test(d)&&!active)p(a,e);else if(/^(un|en|de)/.test(d)&&active)w(a,e);else if(d==E)for(var e in active)active[e]?p(a,e,!0):w(a,e,!0);else if(!c||"toggle"==d){if(!c)a[r]("ifClicked");active?b[l]!==x&&w(a,e):p(a,e)}}function p(a,c,d){var b=a[0],e=a.parent(),g=c==j,H=c==k,m=H?v:g?I:"enabled",r=h(b,m+y(b[l])),L=h(b,
c+y(b[l]));if(!0!==b[c]){if(!d&&c==j&&b[l]==x&&b.name){var p=a.closest("form"),s='input[name="'+b.name+'"]',s=p.length?p.find(s):f(s);s.each(function(){this!==b&&f.data(this,n)&&w(f(this),c)})}H?(b[c]=!0,b[j]&&w(a,j,"force")):(d||(b[c]=!0),g&&b[k]&&w(a,k,!1));J(a,g,c,d)}b[u]&&h(b,z,!0)&&e.find("."+F).css(z,"default");e[t](L||h(b,c));e[A](r||h(b,m)||"")}function w(a,c,d){var b=a[0],e=a.parent(),g=c==j,f=c==k,m=f?v:g?I:"enabled",n=h(b,m+y(b[l])),p=h(b,c+y(b[l]));if(!1!==b[c]){if(f||!d||"force"==d)b[c]=
!1;J(a,g,m,d)}!b[u]&&h(b,z,!0)&&e.find("."+F).css(z,"pointer");e[A](p||h(b,c)||"");e[t](n||h(b,m))}function K(a,c){if(f.data(a,n)){var d=f(a);d.parent().html(d.attr("style",f.data(a,n).s||"")[r](c||""));d.off(".i").unwrap();f(D+'[for="'+a.id+'"]').add(d.closest(D)).off(".i")}}function h(a,c,d){if(f.data(a,n))return f.data(a,n).o[c+(d?"":"Class")]}function y(a){return a.charAt(0).toUpperCase()+a.slice(1)}function J(a,c,d,b){if(!b){if(c)a[r]("ifToggled");a[r]("ifChanged")[r]("if"+y(d))}}var n="iCheck",
F=n+"-helper",x="radio",j="checked",I="un"+j,u="disabled",v="determinate",k="in"+v,E="update",l="type",t="addClass",A="removeClass",r="trigger",D="label",z="cursor",G=/ipad|iphone|ipod|android|blackberry|windows phone|opera mini/i.test(navigator.userAgent);f.fn[n]=function(a,c){var d=":checkbox, :"+x,b=f(),e=function(a){a.each(function(){var a=f(this);b=a.is(d)?b.add(a):b.add(a.find(d))})};if(/^(check|uncheck|toggle|indeterminate|determinate|disable|enable|update|destroy)$/i.test(a))return a=a.toLowerCase(),
e(this),b.each(function(){"destroy"==a?K(this,"ifDestroyed"):C(f(this),!0,a);f.isFunction(c)&&c()});if("object"==typeof a||!a){var g=f.extend({checkedClass:j,disabledClass:u,indeterminateClass:k,labelHover:!0},a),h=g.handle,m=g.hoverClass||"hover",y=g.focusClass||"focus",v=g.activeClass||"active",z=!!g.labelHover,s=g.labelHoverClass||"hover",B=(""+g.increaseArea).replace("%","")|0;if("checkbox"==h||h==x)d=":"+h;-50>B&&(B=-50);e(this);return b.each(function(){K(this);var a=f(this),b=this,c=b.id,d=
-B+"%",e=100+2*B+"%",e={position:"absolute",top:d,left:d,display:"block",width:e,height:e,margin:0,padding:0,background:"#fff",border:0,opacity:0},d=G?{position:"absolute",visibility:"hidden"}:B?e:{position:"absolute",opacity:0},h="checkbox"==b[l]?g.checkboxClass||"icheckbox":g.radioClass||"i"+x,k=f(D+'[for="'+c+'"]').add(a.closest(D)),q=a.wrap('<div class="'+h+'"/>')[r]("ifCreated").parent().append(g.insert),e=f('<ins class="'+F+'"/>').css(e).appendTo(q);a.data(n,{o:g,s:a.attr("style")}).css(d);
g.inheritClass&&q[t](b.className);g.inheritID&&c&&q.attr("id",n+"-"+c);"static"==q.css("position")&&q.css("position","relative");C(a,!0,E);if(k.length)k.on("click.i mouseenter.i mouseleave.i touchbegin.i touchend.i",function(c){var d=c[l],e=f(this);if(!b[u])if("click"==d?C(a,!1,!0):z&&(/ve|nd/.test(d)?(q[A](m),e[A](s)):(q[t](m),e[t](s))),G)c.stopPropagation();else return!1});a.on("click.i focus.i blur.i keyup.i keydown.i keypress.i",function(c){var d=c[l];c=c.keyCode;if("click"==d)return!1;if("keydown"==
d&&32==c)return b[l]==x&&b[j]||(b[j]?w(a,j):p(a,j)),!1;if("keyup"==d&&b[l]==x)!b[j]&&p(a,j);else if(/us|ur/.test(d))q["blur"==d?A:t](y)});e.on("click mousedown mouseup mouseover mouseout touchbegin.i touchend.i",function(d){var c=d[l],e=/wn|up/.test(c)?v:m;if(!b[u]){if("click"==c)C(a,!1,!0);else{if(/wn|er|in/.test(c))q[t](e);else q[A](e+" "+v);if(k.length&&z&&e==m)k[/ut|nd/.test(c)?A:t](s)}if(G)d.stopPropagation();else return!1}})})}return this}})(jQuery);
var Index = function() {"use strict";

	// function to initiate Chart 1
	var runChart1 = function() {
		function randValue() {
			return (Math.floor(Math.random() * (100 + 4000 - 2000))) + 2000;
		};
		function createSeries() {
			var y = date.getFullYear(), m = date.getMonth();
			var firstDay = new Date(y, m, 1);
			var fifthDay = new Date(y, m, 5);
			var tenthDay = new Date(y, m, 10);
			var fifteenthDay = new Date(y, m, 15);
			var twentiethDay = new Date(y, m, 20);
			var twentyfifthDay = new Date(y, m, 25);
			var lastDay = new Date(y, m + 1, 0);

			for(var d = new Date(new Date().setDate(new Date().getDate() - 15)); d <= new Date(); d.setDate(d.getDate() + 1)) {

				series1.push([new Date(d), Math.floor(Math.random() * (1500000 - 450000 + 1)) + 450000]);
				series2.push([new Date(d), Math.random() * (400 - 70) + 70]);
			}
		}

		if($("#chart1 > svg").length) {
			var date = new Date();
			var series1 = [];
			var series2 = [];

			createSeries();

			var data = [{
				"key": "Quantity",
				"bar": true,
				"values": series1
			}, {
				"key": "Price",
				"values": series2
			}];
			nv.addGraph(function() {
				var chart = nv.models.linePlusBarChart().margin({
					top: 15,
					right: 30,
					bottom: 15,
					left: 60
				})
				//We can set x data accessor to use index. Reason? So the bars all appear evenly spaced.
				.x(function(d, i) {
					return i;
				}).y(function(d, i) {
					return d[1];
				}).color(['#DFDFDD', '#E66F6F']);

				chart.xAxis.tickFormat(function(d) {
					var dx = data[0].values[d] && data[0].values[d][0] || 0;
					return d3.time.format('%x')(new Date(dx));
				});

				chart.y1Axis.tickFormat(d3.format(',f'));

				chart.y2Axis.tickFormat(function(d) {
					return '$' + d3.format(',f')(d);
				});

				chart.bars.forceY([0, 2000000]);
				chart.lines.forceY([0, 900]);

				d3.select('#chart1 svg').datum(data).transition().duration(0).call(chart);

				nv.utils.windowResize(chart.update);

				return chart;
			});
		}
	};
	// function to initiate Chart 2
	var runChart2 = function() {
		if($("#chart2 > svg").length) {
			var chart;
			var data = [{
				key: "Stream 1",
				values: [{
					x: 1,
					y: 1
				}]
			}];
			nv.addGraph(function() {

				chart = nv.models.historicalBarChart().margin({
					top: 30,
					right: 0,
					bottom: 40,
					left: 0
				}).color(['#5F8295']);

				chart.x(function(d, i) {
					return d.x;
				});

				chart.xAxis// chart sub-models (ie. xAxis, yAxis, etc) when accessed directly, return themselves, not the parent chart, so need to chain separately
				.tickFormat(d3.format(',.1f'));

				chart.yAxis.tickFormat(d3.format(',.4f'));

				chart.showXAxis(true).showYAxis(false);

				d3.select('#chart2 svg').datum(data).transition().duration(500).call(chart);

				nv.utils.windowResize(chart.update);

				return chart;
			});

			var x = 2;
			var run = true;

			setInterval(function() {
				if(!run)
					return;
				d3.select(".nv-bars").on("mouseover", function() {
					run = false;

				}).on("mouseout", function() {
					run = true;

				});
				var spike = (Math.random() > 0.95) ? 10 : 1;
				data[0].values.push({
					x: x,
					y: Math.random() * spike
				});

				if(data[0].values.length > 10) {
					data[0].values.shift();
				}
				x++;

				chart.update();
			}, 1000);

			d3.select("#start-stop-button").on("click", function() {
				run = !run;
			});
		}
	};
	// function to initiate Chart 3
	var runChart3 = function() {
		if($("#chart3 > svg").length) {
			var data = [{
				"label": "EUR",
				"value": 29.765957771107
			}, {
				"label": "USD",
				"value": 196.45946739256
			}, {
				"label": "Others",
				"value": 92.807804682612
			}];
			nv.addGraph(function() {
				var chart = nv.models.pieChart().margin({
					top: 5,
					right: 0,
					bottom: 0,
					left: 10
				}).x(function(d) {
					return d.label;
				}).y(function(d) {
					return d.value;
				}).showLabels(true)//Display pie labels
				.showLegend(false).labelType("key")//Configure what type of data to show in the label. Can be "key", "value" or "percent"
				.color(['#F3EDED', '#E0CDD1', '#CAABB0']);

				d3.select("#chart3 svg").datum(data).transition().duration(350).call(chart);
				nv.utils.windowResize(chart.update);

				return chart;
			});
		}
	};

	// function to initiate Chart 4
	var runChart4 = function() {
		function randValue() {
			return (Math.floor(Math.random() * (100 + 4000 - 2000))) + 2000;
		};

		function createSeries() {
			var y = date.getFullYear(), m = date.getMonth();
			var firstDay = new Date(y, m, 1);
			var fifthDay = new Date(y, m, 5);
			var tenthDay = new Date(y, m, 10);
			var fifteenthDay = new Date(y, m, 15);
			var twentiethDay = new Date(y, m, 20);
			var twentyfifthDay = new Date(y, m, 25);
			var lastDay = new Date(y, m + 1, 0);

			for(var d = new Date(new Date().setMonth(new Date().getMonth() - 1)); d <= new Date(); d.setDate(d.getDate() + 1)) {

				series1.push([new Date(d), randValue() + Math.floor(Math.random() * 1000)]);
				series2.push([new Date(d), randValue() - Math.floor(Math.random() * 1000)]);
			}
		}

		if($("#chart4 > svg").length) {
			var date = new Date();
			var series1 = [];
			var series2 = [];
			var firstDay, lastDay, fifthDay, tenthDay, fifteenthDay, twentiethDay, twentyfifthDay;

			createSeries();
			var data = [{
				"key": "Page Views",
				"values": series1
			}, {
				"key": "Unique Visits",
				"values": series2
			}];

			nv.addGraph(function() {

				var chart = nv.models.lineChart().margin({
					top: 30,
					right: 0,
					bottom: 20,
					left: 35
				}).x(function(d) {
					return d[0];
				}).y(function(d) {
					return d[1];
				}).forceY([0, 8000]).useInteractiveGuideline(true).color(['#D9534F', '#ffffff']).clipEdge(true);
				var options = {
					showControls: false,
					showLegend: true
				};
				chart.options(options);
				chart.xAxis.tickFormat(function(d) {
					return d3.time.format('%x')(new Date(d));
				}).showMaxMin(false);

				chart.yAxis.tickFormat(d3.format(',f'));
				d3.select('#chart4 svg').datum(data).call(chart);

				nv.utils.windowResize(chart.update);

				return chart;
			});
		}
	};
	// function to initiate Sparkline
	var sparkResize;
	$(window).resize(function(e) {
		clearTimeout(sparkResize);
		sparkResize = setTimeout(runSparkline, 500);
	});
	var runSparkline = function() {

		$(".sparkline-1 span").sparkline([300, 523, 982, 811, 1300, 1125, 1487], {
			type: "bar",
			barColor: "#266866",
			barWidth: "5",
			height: "24",
			tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}}: {{value}}',
			tooltipValueLookups: {
				names: {
					0: 'Sunday',
					1: 'Monday',
					2: 'Tuesday',
					3: 'Wednesday',
					4: 'Thursday',
					5: 'Friday',
					6: 'Saturday'

				}
			}
		});
		$(".sparkline-2 span").sparkline([400, 650, 886, 443, 502, 412, 353], {
			type: "bar",
			barColor: "#ffffff",
			barWidth: "5",
			height: "24",
			tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}}: {{value}}',
			tooltipValueLookups: {
				names: {
					0: 'Sunday',
					1: 'Monday',
					2: 'Tuesday',
					3: 'Wednesday',
					4: 'Thursday',
					5: 'Friday',
					6: 'Saturday'

				}
			}
		});
		$(".sparkline-3 span").sparkline([4879, 6567, 5022, 8890, 9234, 7128, 4811], {
			type: "bar",
			barColor: "#A5E5DD",
			barWidth: "5",
			height: "24",
			tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}}: {{value}}',
			tooltipValueLookups: {
				names: {
					0: 'Sunday',
					1: 'Monday',
					2: 'Tuesday',
					3: 'Wednesday',
					4: 'Thursday',
					5: 'Friday',
					6: 'Saturday'

				}
			}
		});
		$(".sparkline-4 span").sparkline([1122, 1735, 559, 2534, 1600, 2860, 1345, 1987, 2675, 457, 3965, 3765], {
			type: "line",
			lineColor: '#ffffff',
			width: "80%",
			height: "70",
			fillColor: "",
			spotRadius: 4,
			lineWidth: 2,
			resize: true,
			spotColor: '#ffffff',
			minSpotColor: '#ffffff',
			maxSpotColor: '#ffffff',
			highlightSpotColor: '#bf005f',
			highlightLineColor: '#ffffff',
			tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}}: {{y:val}}',
			tooltipValueLookups: {
				names: {
					0: 'January',
					1: 'February',
					2: 'March',
					3: 'April',
					4: 'May',
					5: 'June',
					6: 'July',
					7: 'August',
					8: 'September',
					9: 'October',
					10: 'November',
					11: 'December'

				}
			}
		});
		$(".sparkline-5 span").sparkline([422, 1335, 1059, 2235, 1300, 1860, 1126, 1387, 1675, 1357, 2165, 1765], {
			type: "line",
			lineColor: '#ffffff',
			width: "80%",
			height: "70",
			fillColor: "",
			spotRadius: 4,
			lineWidth: 2,
			resize: true,
			spotColor: '#ffffff',
			minSpotColor: '#ffffff',
			maxSpotColor: '#ffffff',
			highlightSpotColor: '#bf005f',
			highlightLineColor: '#ffffff',
			tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}}: {{y:val}}',
			tooltipValueLookups: {
				names: {
					0: 'January',
					1: 'February',
					2: 'March',
					3: 'April',
					4: 'May',
					5: 'June',
					6: 'July',
					7: 'August',
					8: 'September',
					9: 'October',
					10: 'November',
					11: 'December'

				}
			}
		});
	};
	// function to initiate EasyPieChart
	var runEasyPieChart = function() {
		if(isIE8 || isIE9) {
			if(!Function.prototype.bind) {
				Function.prototype.bind = function(oThis) {
					if( typeof this !== "function") {
						// closest thing possible to the ECMAScript 5 internal IsCallable function
						throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
					}
					var aArgs = Array.prototype.slice.call(arguments, 1), fToBind = this, fNOP = function() {
					}, fBound = function() {
						return fToBind.apply(this instanceof fNOP && oThis ? this : oThis, aArgs.concat(Array.prototype.slice.call(arguments)));
					};
					fNOP.prototype = this.prototype;
					fBound.prototype = new fNOP();
					return fBound;
				};
			}
		}
		var easyDefaultsOptions = {
			animate: 1000,
			lineWidth: 3,
			size: 70,
			onStep: function(from, to, percent) {
				$(this.el).find('.percent').text(Math.round(percent));
			}
		};
		$('.easy-pie-chart .appear').each(function() {
			var configEasy = $.extend({}, easyDefaultsOptions, $(this).data("plugin-options"));
			if($(this).is(':appeared') || isMobile) {
				$(this).easyPieChart(configEasy);
			} else {
				$(this).appear();
				$(this).on("appear", function(event, $all_appeared_elements) {
					$(this).easyPieChart(configEasy);
				});
			}

		});

	};
	// function to initiate Report Range Date
	var runReportRange = function() {
		if($('#reportrange').length) {
			$('#reportrange').daterangepicker({
				ranges: {
					'Today': [moment(), moment()],
					'Yesterday': [moment().subtract('days', 1), moment().subtract('days', 1)],
					'Last 7 Days': [moment().subtract('days', 6), moment()],
					'Last 30 Days': [moment().subtract('days', 29), moment()],
					'This Month': [moment().startOf('month'), moment().endOf('month')],
					'Last Month': [moment().subtract('month', 1).startOf('month'), moment().subtract('month', 1).endOf('month')]
				},
				startDate: moment().subtract('days', 29),
				endDate: moment()
			}, function(start, end) {
				$('#reportrange span').html(start.format('MMM D, YYYY') + ' - ' + end.format('MMM D, YYYY') + ' ');
			});
		}

	};
	// function to animate CoreBox Icons
	var runCoreBoxIcons = function() {
		$(".core-box").on("mouseover", function() {
			$(this).find(".icon-big").addClass("tada animated");
		}).on("mouseleave", function() {
			$(this).find(".icon-big").removeClass("tada animated");
		});
	};
	// function to activate animated Clock and Actual Date
	var runClock = function() {
		function update() {
			var now = moment(), second = now.seconds() * 6, minute = now.minutes() * 6 + second / 60, hour = ((now.hours() % 12) / 12) * 360 + 90 + minute / 12;
			$('#hour').css({
				"-webkit-transform": "rotate(" + hour + "deg)",
				"-moz-transform": "rotate(" + hour + "deg)",
				"-ms-transform": "rotate(" + hour + "deg)",
				"transform": "rotate(" + hour + "deg)"
			});
			$('#minute').css({
				"-webkit-transform": "rotate(" + minute + "deg)",
				"-moz-transform": "rotate(" + minute + "deg)",
				"-ms-transform": "rotate(" + minute + "deg)",
				"transform": "rotate(" + minute + "deg)"
			});
			$('.clock #second').css({
				"-webkit-transform": "rotate(" + second + "deg)",
				"-moz-transform": "rotate(" + second + "deg)",
				"-ms-transform": "rotate(" + second + "deg)",
				"transform": "rotate(" + second + "deg)"
			});
		}

		function timedUpdate() {
			update();
			setTimeout(timedUpdate, 1000);
		}

		timedUpdate();
		$(".actual-date .actual-day").text(moment().format('DD'));
		$(".actual-date .actual-month").text(moment().format('MMMM'));
	};
	// function to activate owlCarousel in Appointments Panel
	var runEventsSlider = function() {
		var owlEvents = $(".appointments .e-slider").data('owlCarousel');
		$(".appointments .owl-next").on("click", function(e) {
			owlEvents.next();
			e.preventDefault();
		});
		$(".appointments .owl-prev").on("click", function(e) {
			owlEvents.prev();
			e.preventDefault();
		});
	};
	return {
		init: function() {
			runChart1();
			runChart2();
			runChart3();
			runChart4();
			runSparkline();
			runReportRange();
			runEasyPieChart();
			runClock();
			runCoreBoxIcons();
			runEventsSlider();
		}
	};
}();

var isIE8 = false,
    isIE9 = false,
    inner = $(".main-wrapper > .inner"),
    supportTransition = true,
    closedbar = $(".closedbar"),
    isMobile = false,
    isIEMobile = false,
    $body = $("body"),
    $windowWidth, $windowHeight, subViews = $(".subviews"),
    sideLeft = $('#pageslide-left'),
    sideRight = $('#pageslide-right'),
    mainNavigation = $('.main-navigation'),
    sidebarWidth = sideLeft.outerWidth(true),
    topBar = $(".topbar"),
    mainContainer = $(".main-container"),
    mainContent = $(".main-content"),
    footer = $(".main-wrapper > footer");
var thisSlider, actualItemWidth, newItemWidth, activeAnimation = false,
    hoverSideBar = false;;

// Debounce Function
(function($, sr) {
    "use strict";
    // debouncing function from John Hann
    // http://unscriptable.com/index.php/2009/03/20/debouncing-javascript-methods/
    var debounce = function(func, threshold, execAsap) {
        var timeout;
        return function debounced() {
            var obj = this,
                args = arguments;

            function delayed() {
                if (!execAsap)
                    func.apply(obj, args);
                timeout = null;
            };

            if (timeout)
                clearTimeout(timeout);
            else if (execAsap)
                func.apply(obj, args);

            timeout = setTimeout(delayed, threshold || 100);
        };
    };
    // smartresize
    jQuery.fn[sr] = function(fn) {
        return fn ? this.on('resize', debounce(fn)) : this.trigger(sr);
    };

})(jQuery, 'espressoResize');

//Main Function
var Main = function() {
    "use strict";
    //function to init app
    var runInit = function() {
        // Detection for IE Version
        if (/MSIE (\d+\.\d+);/.test(navigator.userAgent)) {
            var ieversion = new Number(RegExp.$1);
            if (ieversion == 8) {
                isIE8 = true;
                $body.addClass('isIE8');
            } else if (ieversion == 9) {
                isIE9 = true;
                $body.addClass('isIE9');
            }
        }
        // Detection for Mobile Device
        if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
            isMobile = true;
            $body.addClass('isMobile');
        };
        // Detection for CSS Transitions Support
        var thisBody = document.body || document.documentElement,
            thisStyle = thisBody.style;
        supportTransition = thisStyle.transition !== undefined || thisStyle.WebkitTransition !== undefined || thisStyle.MozTransition !== undefined || thisStyle.MsTransition !== undefined || thisStyle.OTransition !== undefined;
        // active perfectScrollbar only in desktop devices
        if ($body.hasClass("isMobile") == false && mainNavigation.length) {
            mainNavigation.perfectScrollbar({
                wheelSpeed: 50,
                minScrollbarLength: 20,
                suppressScrollX: true
            });
            $(".right-wrapper").perfectScrollbar({
                wheelSpeed: 50,
                minScrollbarLength: 20,
                suppressScrollX: true
            });

        }
        // clones the horizontal menu and inserts it into left sidebar for mobile devices
        if ($("#horizontal-menu").length) {
            if ($(".main-navigation-menu").length) {
                $("#horizontal-menu").find(".nav").clone().removeClass("nav navbar-nav").addClass("main-navigation-menu core-menu").find("li.dropdown").removeClass("dropdown").find("a").removeClass("dropdown-toggle").removeAttr("data-toggle").end().end().find("ul.dropdown-menu").removeClass("dropdown-menu").addClass("sub-menu").end().addClass("hidden-md hidden-lg").insertBefore(".main-navigation-menu");
            } else if ($(".user-profile").length) {
                $("#horizontal-menu").find(".nav").clone().removeClass("nav navbar-nav").addClass("main-navigation-menu core-menu").find("li.dropdown").removeClass("dropdown").find("a").removeClass("dropdown-toggle").removeAttr("data-toggle").end().end().find("ul.dropdown-menu").removeClass("dropdown-menu").addClass("sub-menu").end().addClass("hidden-md hidden-lg").insertAfter(".user-profile");
            } else {
                $("#horizontal-menu").find(".nav").clone().removeClass("nav navbar-nav").addClass("main-navigation-menu core-menu").find("li.dropdown").removeClass("dropdown").find("a").removeClass("dropdown-toggle").removeAttr("data-toggle").end().end().find("ul.dropdown-menu").removeClass("dropdown-menu").addClass("sub-menu").end().addClass("hidden-md hidden-lg").prependTo(".main-navigation");
            }

        }

        // set blockUI options
        if ($.blockUI) {
            $.blockUI.defaults.css.border = 'none';
            $.blockUI.defaults.css.padding = '20px 5px';
            $.blockUI.defaults.css.width = '20%';
            $.blockUI.defaults.css.left = '40%';
            $.blockUI.defaults.overlayCSS.backgroundColor = '#DDDDDD';
        }

        // Add Fade Animation to Dropdown
        $('.dropdown').on('show.bs.dropdown', function(e) {
            $(this).find('.dropdown-menu').first().stop(true, true).fadeIn(300);
        });
        $('.dropdown').on('hide.bs.dropdown', function(e) {
            $(this).find('.dropdown-menu').first().stop(true, true).fadeOut(300);
        });

        // change closebar height when footer appear
        if ($.fn.appear) {
            if (isMobile == false) {
                footer.appear();
                footer.on("appear", function(event, $all_appeared_elements) {

                    closedbar.css({
                        bottom: (footer.outerHeight(true) + 1) + "px"
                    });
                });
                footer.on("disappear", function(event, $all_disappeared_elements) {

                    closedbar.css({
                        bottom: 1 + "px"
                    });
                });
            }
        }

    };
    //function to get viewport/window size (width and height)
    var viewport = function() {
        var e = window,
            a = 'inner';
        if (!('innerWidth' in window)) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        return {
            width: e[a + 'Width'],
            height: e[a + 'Height']
        };
    };
    //function to close searchbox, pageslide-left and pageslide-right when the user clicks outside of them
    var documentEvents = function() {
        $("html").click(function(e) {
            if (!e.isDefaultPrevented()) {
                if ($('.search-box').is(":visible")) {
                    $('.search-box').velocity({
                        scale: 0.9,
                        opacity: 0
                    }, 400, 'easeInBack', function() {
                        $(this).hide();
                    });
                }

                if ($body.hasClass("right-sidebar-open") && !hoverSideBar && !isMobile) {
                    $(".sb-toggle-right").trigger("click");
                } else if ($body.hasClass("sidebar-mobile-open") && !hoverSideBar && !isMobile) {
                    $("header .sb-toggle-left").trigger("click");
                }
            }
        });
        if (isMobile) {
            $("html").swipe({
                swipeLeft: function(event, direction, distance, duration, fingerCount) {
                    if ($body.hasClass("sidebar-mobile-open")) {
                        $("header .sb-toggle-left").trigger("click");
                    }
                },
                swipeRight: function(event, direction, distance, duration, fingerCount) {
                    if ($body.hasClass("right-sidebar-open")) {
                        $(".sb-toggle-right").trigger("click");
                    }
                }
            });
        }

    };

    // function to handle SlideBar Toggle
    var runSideBarToggle = function() {
        $(".sb_toggle").click(function() {
            var sb_toggle = $(this);
            $("#slidingbar").slideToggle("fast", function() {
                if (sb_toggle.hasClass('open')) {
                    sb_toggle.removeClass('open');
                } else {
                    sb_toggle.addClass('open');
                }
            });
        });
    };
    // function to adjust the template elements based on the window size
    var runElementsPosition = function() {
        $windowWidth = viewport().width;
        $windowHeight = viewport().height;
        runContainerHeight();

    };

    //function to adapt the Main Content height to the Main Navigation height
    var runContainerHeight = function() {
        if (subViews.is(':visible')) {
            $('.main-container').css({

                'max-height': $windowHeight - topBar.outerHeight(true),
                'min-height': $windowHeight - topBar.outerHeight(true),

            });
        }
        if ($("#slidingbar-area").is(':visible')) {
            $("#slidingbar-area").css({
                'max-height': $windowHeight
            });
        }
        if ($windowWidth > 991) {
            mainNavigation.css({
                height: $windowHeight - topBar.outerHeight(true) - $(".slide-tools").outerHeight(true)
            });
            $(".navbar-content").css({
                height: $windowHeight - topBar.outerHeight(true)
            });
        } else {
            mainNavigation.css({
                height: $windowHeight - $(".slide-tools").outerHeight(true)
            });
            $(".navbar-content").css({
                height: $windowHeight
            });
        }

        $(".right-wrapper").css({
            height: $windowHeight
        });

        if ($body.hasClass("isMobile") == false && mainNavigation.length) {
            mainNavigation.perfectScrollbar('update');
            $(".right-wrapper").perfectScrollbar('update');
        }
        if ($("#horizontal-menu").length) {
            mainContent.css({
                "min-height": $windowHeight - topBar.outerHeight(true) - $("#horizontal-menu").outerHeight(true) - footer.outerHeight(true)
            });
        } else {
            mainContent.css({
                "min-height": $windowHeight - topBar.outerHeight(true) - footer.outerHeight(true)
            });
        }

        if (subViews.is(":visible")) {
            subViews.css({
                height: $windowHeight - topBar.outerHeight(true) - $(".toolbar").outerHeight(true)
            });
        }

    };
    // function to activate the ToDo list, if present
    var runToDoAction = function() {
        if ($(".todo-actions").length) {
            $(".todo-actions > i").click(function() {
                if ($(this).hasClass("fa-square-o") || $(this).hasClass("icon-check-empty")) {

                    $(this).removeClass("fa-square-o").addClass("fa-check-square-o").parent().find("span").css({
                        opacity: .25
                    }).end().find(".todo-tools").hide().end().parent().find(".desc").css("text-decoration", "line-through");
                } else {
                    $(this).removeClass("fa-check-square-o").addClass("fa-square-o").parent().find("span").css({
                        opacity: 1
                    }).end().find(".todo-tools").show().end().parent().find(".desc").css("text-decoration", "none");
                }
                return !1;
            });
        }
    };
    //capture the user's attention with some animation
    var runAnimatedElements = function() {
        /*
		 if($('.messages-count').length) {
		 setTimeout(function() {
		 $('.messages-count').removeClass('hide');
		 $('.messages-count').addClass('animated bounceIn');
		 }, 4000);
		 }
		 */
        if ($('.tooltip-notification').length) {
            setTimeout(function() {
                $('.tooltip-notification').removeClass('hide');
                $('.tooltip-notification').addClass('animated fadeIn');
            }, 5000);
            setTimeout(function() {
                $('.tooltip-notification').velocity({
                    opacity: 0
                }, 300, 'ease', function() {
                    $(this).removeClass('animated bounceIn').addClass('hide');
                });
            }, 8000);
        }

        if ($('.notifications-count').length) {
            setTimeout(function() {
                $('.notifications-count').removeClass('hide');
                $('.notifications-count').addClass('animated bounceIn');
            }, 10000);
        }
    };
    //function to run quick chat
    var runQuickChat = function() {
        $("#users").css({
            minHeight: $("#users .users-list").outerHeight()
        });
        $(".users-list .media a").on("click", function(e) {
            $(this).closest(".tab-pane").find(".user-chat").show().end().css({
                right: sideRight.outerWidth()
            });
            $(".right-wrapper").perfectScrollbar('update');
            e.preventDefault();
        });
        $(".user-chat .sidebar-back").on("click", function(e) {
            $(this).closest(".tab-pane").find(".user-chat").hide().end().css({
                right: 0
            });
            $(".right-wrapper").perfectScrollbar('update');
            e.preventDefault();
        });

        $('#sidebar-tab a').on('shown.bs.tab', function(e) {

            $(".right-wrapper").perfectScrollbar('update');
        });
    };
    //Search Box Function
    var setSearchMenu = function() {
        $('.menu-search > a').on('click', function(e) {
            if ($('.search-box').is(":hidden")) {
                $('.search-box').css({
                    scale: 0.8,
                    opacity: 0,
                    display: 'block'
                }).velocity({
                    scale: 1,
                    opacity: 1
                }, 400, 'easeOutBack');
            } else {
                $('.search-box').velocity({
                    scale: 0.9,
                    opacity: 0
                }, 400, 'easeInBack', function() {
                    $(this).hide();
                });
            }
            e.preventDefault();

        });
        $('.menu-search').on('click', function(e) {
            e.preventDefault();
        });
    };
    //function to activate the Tooltips, if present
    var runTooltips = function() {
        if ($(".tooltips").length) {
            $('.tooltips').tooltip();
        }
    };
    //function to activate the Popovers, if present
    var runPopovers = function() {
        if ($(".popovers").length) {
            $('.popovers').popover();
        }
    };
    // function to allow a button or a link to open a tab
    var runShowTab = function(e) {
        if ($(".show-tab").length) {
            $('.show-tab').on('click', function(e) {
                e.preventDefault();
                var tabToShow = $(this).attr("href");
                if ($(tabToShow).length) {
                    $('a[href="' + tabToShow + '"]').tab('show');
                }
            });
        };
        if (getParameterByName('tabId').length) {
            $('a[href="#' + getParameterByName('tabId') + '"]').tab('show');
        }
    };
    // function to enable panel scroll with perfectScrollbar
    var runPanelScroll = function() {
        if ($(".panel-scroll").length && $body.hasClass("isMobile") == false) {
            $('.panel-scroll').perfectScrollbar({
                wheelSpeed: 50,
                minScrollbarLength: 20,
                suppressScrollX: true
            });
        }
    };
    //function to activate the panel tools
    var runModuleTools = function() {
        // fullscreen
        $('body').on('click', '.panel-expand', function(e) {
            e.preventDefault();
            $('.panel-tools > a, .panel-tools .dropdown').hide();

            if ($('.full-white-backdrop').length == 0) {
                $body.append('<div class="full-white-backdrop"></div>');
            }
            var backdrop = $('.full-white-backdrop');
            var wbox = $(this).parent().parents('.panel');
            wbox.attr('style', '');
            if (wbox.hasClass('panel-full-screen')) {
                backdrop.fadeIn(200, function() {
                    $('.panel-tools > .tmp-tool').remove();
                    $('.panel-tools > a, .panel-tools .dropdown').show();
                    wbox.removeClass('panel-full-screen');
                    backdrop.fadeOut(200, function() {
                        backdrop.remove();
                        $(window).trigger('resize');
                    });
                });
            } else {

                backdrop.fadeIn(200, function() {

                    $('.panel-tools').append("<a class='panel-expand tmp-tool' href='#'><i class='fa fa-compress'></i></a>");
                    backdrop.fadeOut(200, function() {
                        backdrop.hide();
                    });
                    wbox.addClass('panel-full-screen').css({
                        'max-height': $windowHeight,
                        'overflow': 'auto'
                    });
                    $(window).trigger('resize');
                });
            }
        });
        // panel close
        $('body').on('click', '.panel-close', function(e) {
            $(this).parents(".panel").fadeOut();
            e.preventDefault();
        });
        // panel refresh
        $('body').on('click', '.panel-refresh', function(e) {
            var el = $(this).parents(".panel");
            el.block({
                overlayCSS: {
                    backgroundColor: '#fff'
                },
                message: '<i class="fa fa-spinner fa-spin"></i>',
                css: {
                    border: 'none',
                    color: '#333',
                    background: 'none'
                }
            });
            window.setTimeout(function() {
                el.unblock();
            }, 1000);
            e.preventDefault();
        });
        // panel collapse
        $('body').on('click', '.panel-collapse', function(e) {
            e.preventDefault();
            var el = $(this);
            var bodyPanel = jQuery(this).parent().closest(".panel").children(".panel-body");
            if ($(this).hasClass("collapses")) {
                bodyPanel.slideUp(200, function() {
                    el.addClass("expand").removeClass("collapses").children("span").text("Expand").end().children("i").addClass("fa-rotate-180");
                });
            } else {
                bodyPanel.slideDown(200, function() {
                    el.addClass("collapses").removeClass("expand").children("span").text("Collapse").end().children("i").removeClass("fa-rotate-180");
                });
            }
        });
    };
    //function to activate the main menu functionality
    var runNavigationMenu = function() {
        if ($("body").hasClass("single-page") == false) {
            $('.main-navigation-menu > li.active').addClass('open');
            $('.main-navigation-menu > li a').on('click', function() {

                if ($(this).parent().children('ul').hasClass('sub-menu') && ((!$body.hasClass('navigation-small') || $windowWidth < 767) || !$(this).parent().parent().hasClass('main-navigation-menu'))) {
                    if (!$(this).parent().hasClass('open')) {
                        $(this).parent().addClass('open');
                        $(this).parent().parent().children('li.open').not($(this).parent()).not($('.main-navigation-menu > li.active')).removeClass('open').children('ul').slideUp(200);
                        $(this).parent().children('ul').slideDown(200, function() {
                            if (mainNavigation.height() > $(".main-navigation-menu").outerHeight()) {

                                mainNavigation.scrollTo($(this).parent("li"), 300, {
                                    onAfter: function() {
                                        if ($body.hasClass("isMobile") == false) {
                                            mainNavigation.perfectScrollbar('update');
                                        }
                                    }
                                });
                            } else {

                                mainNavigation.scrollTo($(this).parent("li"), 300, {
                                    onAfter: function() {
                                        if ($body.hasClass("isMobile") == false) {
                                            mainNavigation.perfectScrollbar('update');
                                        }
                                    }
                                });
                            }
                        });
                    } else {
                        if (!$(this).parent().hasClass('active')) {
                            $(this).parent().parent().children('li.open').not($('.main-navigation-menu > li.active')).removeClass('open').children('ul').slideUp(200, function() {
                                if (mainNavigation.height() > $(".main-navigation-menu").outerHeight()) {
                                    mainNavigation.scrollTo(0, 300, {
                                        onAfter: function() {
                                            if ($body.hasClass("isMobile") == false) {
                                                mainNavigation.perfectScrollbar('update');
                                            }
                                        }
                                    });
                                } else {
                                    mainNavigation.scrollTo($(this).parent("li").closest("ul").children("li:eq(0)"), 300, {
                                        onAfter: function() {
                                            if ($body.hasClass("isMobile") == false) {
                                                mainNavigation.perfectScrollbar('update');
                                            }
                                        }
                                    });
                                }
                            });
                        } else {
                            $(this).parent().parent().children('li.open').removeClass('open').children('ul').slideUp(200, function() {
                                if (mainNavigation.height() > $(".main-navigation-menu").outerHeight()) {
                                    mainNavigation.scrollTo(0, 300, {
                                        onAfter: function() {
                                            if ($body.hasClass("isMobile") == false) {
                                                mainNavigation.perfectScrollbar('update');
                                            }
                                        }
                                    });
                                } else {
                                    mainNavigation.scrollTo($(this).parent("li"), 300, {
                                        onAfter: function() {
                                            if ($body.hasClass("isMobile") == false) {
                                                mainNavigation.perfectScrollbar('update');
                                            }
                                        }
                                    });
                                }
                            });
                        }
                    }
                } else {
                    $(this).parent().addClass('active');
                }
            });
        } else {
            var url, ajaxContainer = $("#ajax-content");
            var start = $('.main-navigation-menu li.start');
            if (start.length) {
                start.addClass("active");
                if (start.closest('ul').hasClass('sub-menu')) {
                    start.closest('ul').parent('li').addClass('active open');
                }
                url = start.children("a").attr("href");
                ajaxLoader(url, ajaxContainer);
            }
            $('.main-navigation-menu > li.active').addClass('open');
            $('.main-navigation-menu > li a').on('click', function(e) {
                e.preventDefault();
                var $this = $(this);

                if ($this.parent().children('ul').hasClass('sub-menu') && (!$('body').hasClass('navigation-small') || !$this.parent().parent().hasClass('main-navigation-menu'))) {
                    if (!$this.parent().hasClass('open')) {
                        $this.parent().addClass('open');
                        $this.parent().parent().children('li.open').not($this.parent()).not($('.main-navigation-menu > li.active')).removeClass('open').children('ul').slideUp(200);
                        $this.parent().children('ul').slideDown(200, function() {
                            runContainerHeight();
                        });
                    } else {
                        if (!$this.parent().hasClass('active')) {
                            $this.parent().parent().children('li.open').not($('.main-navigation-menu > li.active')).removeClass('open').children('ul').slideUp(200, function() {
                                runContainerHeight();
                            });
                        } else {
                            $this.parent().parent().children('li.open').removeClass('open').children('ul').slideUp(200, function() {
                                runContainerHeight();
                            });
                        }
                    }
                } else {

                    $('.main-navigation-menu ul.sub-menu li').removeClass('active');
                    $this.parent().addClass('active');
                    var closestUl = $this.parent('li').closest('ul');
                    if (closestUl.hasClass('main-navigation-menu')) {
                        $('.main-navigation-menu > li.active').removeClass('active').removeClass('open').children('ul').slideUp(200);
                        $this.parents('li').addClass('active');
                    } else if (!closestUl.parent('li').hasClass('active') && !closestUl.parent('li').closest('ul').hasClass('sub-menu')) {
                        $('.main-navigation-menu > li.active').removeClass('active').removeClass('open').children('ul').slideUp(200);
                        $this.parent('li').closest('ul').parent('li').addClass('active');
                    } else {

                        if (closestUl.parent('li').closest('ul').hasClass('sub-menu')) {
                            if (!closestUl.parents('li.open').hasClass('active')) {
                                $('.main-navigation-menu > li.active').removeClass('active').removeClass('open').children('ul').slideUp(200);
                                closestUl.parents('li.open').addClass('active');
                            }
                        }

                    }
                    url = $(this).attr("href");
                    ajaxLoader(url, ajaxContainer);
                };
            });
        }
    };
    // function to load content with ajax
    var ajaxLoader = function(url, element) {
        element.removeClass("fadeIn shake");
        var backdrop = $('.ajax-white-backdrop');

        $(".main-container").append('<div class="ajax-white-backdrop"></div>');
        backdrop.show();

        if ($body.hasClass("sidebar-mobile-open")) {
            var configAnimation, extendOptions, globalOptions = {
                duration: 200,
                easing: "ease",
                mobileHA: true,
                progress: function() {
                    activeAnimation = true;
                }
            };
            extendOptions = {
                complete: function() {
                    inner.attr('style', '').removeClass("inner-transform");
                    // remove inner-transform (hardware acceleration) for restore z-index
                    $body.removeClass("sidebar-mobile-open");
                    loadPage(url, element);
                    activeAnimation = false;
                }
            };
            configAnimation = $.extend({}, extendOptions, globalOptions);

            inner.velocity({
                translateZ: 0,
                translateX: [-sidebarWidth, 0]
            }, configAnimation);
        } else {
            loadPage(url, element);
        }

        function loadPage(url, element) {
            $.ajax({
                type: "GET",
                cache: false,
                url: url,
                dataType: "html",
                success: function(data) {
                    backdrop.remove();

                    element.html(data).addClass("fadeIn");

                },
                error: function(xhr, ajaxOptions, thrownError) {
                    backdrop.remove();
                    element.html('<h4>Could not load the requested content.</h4>').addClass("shake");

                }
            });
        };
    };
    // function to initiate owlCarousel
    var runESlider = function(options) {
        $(".e-slider").each(function() {
            var slider = $(this);
            var setAutoPlay = !isMobile;
            // AutoPlay False for mobile devices
            var defaults = {
                mouseDrag: false,
                touchDrag: true,
                slideSpeed: 300,
                paginationSpeed: 400,
                singleItem: true,
                navigation: false,
                autoPlay: setAutoPlay
            };
            var config = $.extend({}, defaults, options, slider.data("plugin-options"));

            // Initialize Slider
            slider.owlCarousel(config);
        });
    };

    // function to activate Selectpicker, if present
    var runSelecticker = function() {
        if ($.fn.selectpicker) {
            $('.selectpicker').selectpicker();
        }
    };
    // function to activate moment#fromNow
    var runTimeStamp = function() {
        $(".timestamp").each(function() {
            var startOfPeriod = moment($(this).attr("title"));
            $(this).text(moment(startOfPeriod).startOf('hour').fromNow());
        });
    };
    //function to Right and Left PageSlide
    var runToggleSideBars = function() {
        var configAnimation, extendOptions, globalOptions = {
            duration: 150,
            easing: "ease",
            mobileHA: true,
            progress: function() {
                activeAnimation = true;
            }
        };
        $("#pageslide-left, #pageslide-right").on("mouseover", function(e) {
            hoverSideBar = true;
        }).on("mouseleave", function(e) {
            hoverSideBar = false;
        });
        $(".sb-toggle-left, .closedbar").on("click", function(e) {
            if (activeAnimation == false) {
                if ($windowWidth > 991) {
                    $body.removeClass("sidebar-mobile-open");
                    if ($body.hasClass("sidebar-close")) {
                        if ($body.hasClass("layout-boxed") || $body.hasClass("isMobile")) {
                            $body.removeClass("sidebar-close");
                            closedbar.removeClass("open");
                            $(window).trigger('resize');
                        } else {
                            closedbar.removeClass("open").hide();
                            closedbar.css({
                                //translateZ: 0,
                                left: -closedbar.width()
                            });

                            extendOptions = {
                                complete: function() {
                                    $body.removeClass("sidebar-close");
                                    $(".main-container, #pageslide-left, #footer .footer-inner, #horizontal-menu .container, .closedbar").attr('style', '');
                                    $(window).trigger('resize');
                                    activeAnimation = false;
                                }
                            };
                            configAnimation = $.extend({}, extendOptions, globalOptions);
                            $(".main-container, footer .footer-inner, #horizontal-menu .container").velocity({
                                //translateZ: 0,
                                marginLeft: sidebarWidth
                            }, configAnimation);

                        }

                    } else {
                        if ($body.hasClass("layout-boxed") || $body.hasClass("isMobile")) {
                            $body.addClass("sidebar-close");
                            closedbar.addClass("open");
                            $(window).trigger('resize');
                        } else {
                            sideLeft.css({
                                zIndex: 0

                            });
                            extendOptions = {
                                complete: function() {
                                    closedbar.show().velocity({
                                        //translateZ: 0,
                                        left: 0
                                    }, 100, 'ease', function() {
                                        activeAnimation = false;
                                        closedbar.addClass("open");
                                        $body.addClass("sidebar-close");
                                        $(".main-container, footer .footer-inner, #horizontal-menu .container, .closedbar").attr('style', '');
                                        $(window).trigger('resize');
                                    });
                                }
                            };
                            configAnimation = $.extend({}, extendOptions, globalOptions);
                            $(".main-container, footer .footer-inner, #horizontal-menu .container").velocity({
                                //translateZ: 0,
                                marginLeft: 0
                            }, configAnimation);
                        }
                    }

                } else {
                    if ($body.hasClass("sidebar-mobile-open")) {
                        if (supportTransition) {
                            extendOptions = {
                                complete: function() {
                                    inner.attr('style', '').removeClass("inner-transform");
                                    // remove inner-transform (hardware acceleration) for restore z-index
                                    $body.removeClass("sidebar-mobile-open");
                                    activeAnimation = false;
                                }
                            };
                            configAnimation = $.extend({}, extendOptions, globalOptions);

                            inner.velocity({
                                translateZ: 0,
                                translateX: [-sidebarWidth, 0]
                            }, configAnimation);
                        } else {
                            $body.removeClass("sidebar-mobile-open");
                        }
                    } else {
                        if (supportTransition) {
                            inner.addClass("inner-transform");
                            // add inner-transform for hardware acceleration
                            extendOptions = {
                                complete: function() {
                                    inner.attr('style', '');
                                    $body.addClass("sidebar-mobile-open");
                                    activeAnimation = false;
                                }
                            };
                            configAnimation = $.extend({}, extendOptions, globalOptions);
                            inner.velocity({
                                translateZ: 0,
                                translateX: [sidebarWidth, 0]
                            }, configAnimation);
                        } else {
                            $body.addClass("sidebar-mobile-open");
                        }

                    }
                }
            }
            e.preventDefault();
        });
        $(".sb-toggle-right").on("click", function(e) {
            if (activeAnimation == false) {
                if ($windowWidth > 991) {
                    $body.removeClass("sidebar-mobile-open");
                }
                if ($body.hasClass("right-sidebar-open")) {
                    if (supportTransition) {
                        extendOptions = {
                            complete: function() {
                                inner.attr('style', '').removeClass("inner-transform");
                                // remove inner-transform (hardware acceleration) for restore z-index
                                $body.removeClass("right-sidebar-open");
                                activeAnimation = false;
                            }
                        };
                        configAnimation = $.extend({}, extendOptions, globalOptions);
                        inner.velocity({
                            translateZ: 0,
                            translateX: [sidebarWidth, 0]
                        }, configAnimation);
                    } else {
                        $body.removeClass("right-sidebar-open");
                    }
                } else {
                    if (supportTransition) {
                        inner.addClass("inner-transform");
                        // add inner-transform for hardware acceleration
                        extendOptions = {
                            complete: function() {
                                inner.attr('style', '');
                                $body.addClass("right-sidebar-open");
                                activeAnimation = false;
                            }
                        };
                        configAnimation = $.extend({}, extendOptions, globalOptions);
                        inner.velocity({
                            translateZ: 0,
                            translateX: [-sidebarWidth, 0]
                        }, configAnimation);
                    } else {
                        $body.addClass("right-sidebar-open");
                    }
                }
            }
            e.preventDefault();
        });
    };
    // function to activate ClosedBar Button
    var runClosedBarButton = function() {
        var t;
        closedbar.mouseover(function() {
            if ($body.hasClass("layout-boxed") == false && $body.hasClass("isMobile") == false && closedbar.hasClass("open")) {
                t = setTimeout(function() {
                    closedbar.velocity({
                        left: -closedbar.width()
                    }, 100, 'ease');
                    sideLeft.css({
                        left: -sidebarWidth,
                        zIndex: 1021
                    }).velocity({
                        left: 0

                    }, 200, 'ease');
                }, 800);
            }

        }).mouseleave(function() {

            if ($body.hasClass("layout-boxed") == false && $body.hasClass("isMobile") == false) {
                clearTimeout(t);
            }
        });
        sideLeft.mouseleave(function() {
            if ($body.hasClass("sidebar-close") && closedbar.hasClass("open") && $body.hasClass("isMobile") == false) {
                sideLeft.velocity({
                    left: -sidebarWidth

                }, 200, 'ease', function() {
                    closedbar.velocity({
                        left: 0
                    }, 200, 'ease');
                    sideLeft.css({
                        left: 0,
                        zIndex: 0
                    });
                });
            }
        });
    };
    // function to activate the Go-Top button
    var runGoTop = function(e) {
        $('.go-top').on('click', function(e) {
            $("html, body").animate({
                scrollTop: 0
            }, "slow");
            e.preventDefault();
        });
    };
    // function to refresh owlCarousel
    var runRefreshSliders = function() {
        $(".e-slider").each(function() {
            var slider = $(this).data('owlCarousel');
            slider.reinit();
        });
    };
    //function to avoid closing the dropdown on click
    var runDropdownEnduring = function() {
        if ($('.dropdown-menu.dropdown-enduring').length) {
            $('.dropdown-menu.dropdown-enduring').click(function(event) {
                event.stopPropagation();
            });
        }
    };
    //function to return the querystring parameter with a given name.
    var getParameterByName = function(name) {
        name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    };
    //function to activate the iCheck Plugin
    var runCustomCheck = function() {
        if ($.fn.iCheck) {
            if ($('input[type="checkbox"]').length || $('input[type="radio"]').length) {
                $('input[type="checkbox"].grey, input[type="radio"].grey').iCheck({
                    checkboxClass: 'icheckbox_minimal-grey',
                    radioClass: 'iradio_minimal-grey',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].red, input[type="radio"].red').iCheck({
                    checkboxClass: 'icheckbox_minimal-red',
                    radioClass: 'iradio_minimal-red',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].green, input[type="radio"].green').iCheck({
                    checkboxClass: 'icheckbox_minimal-green',
                    radioClass: 'iradio_minimal-green',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].teal, input[type="radio"].teal').iCheck({
                    checkboxClass: 'icheckbox_minimal-aero',
                    radioClass: 'iradio_minimal-aero',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].orange, input[type="radio"].orange').iCheck({
                    checkboxClass: 'icheckbox_minimal-orange',
                    radioClass: 'iradio_minimal-orange',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].purple, input[type="radio"].purple').iCheck({
                    checkboxClass: 'icheckbox_minimal-purple',
                    radioClass: 'iradio_minimal-purple',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].yellow, input[type="radio"].yellow').iCheck({
                    checkboxClass: 'icheckbox_minimal-yellow',
                    radioClass: 'iradio_minimal-yellow',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].square-black, input[type="radio"].square-black').iCheck({
                    checkboxClass: 'icheckbox_square',
                    radioClass: 'iradio_square',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].square-grey, input[type="radio"].square-grey').iCheck({
                    checkboxClass: 'icheckbox_square-grey',
                    radioClass: 'iradio_square-grey',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].square-red, input[type="radio"].square-red').iCheck({
                    checkboxClass: 'icheckbox_square-red',
                    radioClass: 'iradio_square-red',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].square-green, input[type="radio"].square-green').iCheck({
                    checkboxClass: 'icheckbox_square-green',
                    radioClass: 'iradio_square-green',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].square-teal, input[type="radio"].square-teal').iCheck({
                    checkboxClass: 'icheckbox_square-aero',
                    radioClass: 'iradio_square-aero',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].square-orange, input[type="radio"].square-orange').iCheck({
                    checkboxClass: 'icheckbox_square-orange',
                    radioClass: 'iradio_square-orange',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].square-purple, input[type="radio"].square-purple').iCheck({
                    checkboxClass: 'icheckbox_square-purple',
                    radioClass: 'iradio_square-purple',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].square-yellow, input[type="radio"].square-yellow').iCheck({
                    checkboxClass: 'icheckbox_square-yellow',
                    radioClass: 'iradio_square-yellow',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].flat-black, input[type="radio"].flat-black').iCheck({
                    checkboxClass: 'icheckbox_flat',
                    radioClass: 'iradio_flat',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].flat-grey, input[type="radio"].flat-grey').iCheck({
                    checkboxClass: 'icheckbox_flat-grey',
                    radioClass: 'iradio_flat-grey',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                    checkboxClass: 'icheckbox_flat-red',
                    radioClass: 'iradio_flat-red',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].flat-green, input[type="radio"].flat-green').iCheck({
                    checkboxClass: 'icheckbox_flat-green',
                    radioClass: 'iradio_flat-green',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].flat-teal, input[type="radio"].flat-teal').iCheck({
                    checkboxClass: 'icheckbox_flat-aero',
                    radioClass: 'iradio_flat-aero',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].flat-orange, input[type="radio"].flat-orange').iCheck({
                    checkboxClass: 'icheckbox_flat-orange',
                    radioClass: 'iradio_flat-orange',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].flat-purple, input[type="radio"].flat-purple').iCheck({
                    checkboxClass: 'icheckbox_flat-purple',
                    radioClass: 'iradio_flat-purple',
                    increaseArea: '10%' // optional
                });
                $('input[type="checkbox"].flat-yellow, input[type="radio"].flat-yellow').iCheck({
                    checkboxClass: 'icheckbox_flat-yellow',
                    radioClass: 'iradio_flat-yellow',
                    increaseArea: '10%' // optional
                });
            };
        }
    };
    //Set of functions for Style Selector
    var runStyleSelector = function() {
        setColorScheme();
        setLayoutStyle();
        setHeaderStyle();
        setSideBarStyle();
        setFooterStyle();
        setBoxedBackgrounds();
    };

    var setColorScheme = function() {
        $('.icons-color a').on('click', function() {
            $('.icons-color img').each(function() {
                $(this).removeClass('active');
            });
            $(this).find('img').addClass('active');
            if ($('#skin_color').attr("rel") == "stylesheet/less") {
                $('#skin_color').next('style').remove();
                $('#skin_color').attr("rel", "stylesheet");

            }
            $('#skin_color').attr("href", "assets/css/themes/theme-" + $(this).attr('id') + ".css");
            switch ($(this).attr('id')) {
                case "style3":
                    $(".navbar-brand img").attr("src", "assets/images/logo_dark.png");
                    break;
                default:
                    $(".navbar-brand img").attr("src", "assets/images/logo.png");
                    break;
            };
        });
    };
    var setBoxedBackgrounds = function() {
        $('.boxed-patterns a').on('click', function() {
            if ($body.hasClass('layout-boxed')) {
                var classes = $body.attr("class").split(" ").filter(function(item) {
                    return item.indexOf("bg_style_") === -1 ? item : "";
                });
                $body.attr("class", classes.join(" "));
                $('.boxed-patterns img').each(function() {
                    $(this).removeClass('active');
                });
                $(this).find('img').addClass('active');
                $body.addClass($(this).attr('id'));
            } else {
                alert('Select boxed layout');
            }
        });
    };
    var setLayoutStyle = function() {
        $('select[name="layout"]').change(function() {
            if ($('select[name="layout"] option:selected').val() == 'boxed') {
                $body.addClass('layout-boxed');
                mainContainer.css({
                    marginLeft: ""
                });
                $(window).trigger('resize');
            } else {
                $body.removeClass('layout-boxed');
                closedbar.css({
                    transform: ""
                });
                $(window).trigger('resize');
            }

            $(".sb-toggle-right").trigger("click");
        });
    };
    var setHeaderStyle = function() {
        $('select[name="header"]').change(function() {
            if ($('select[name="header"] option:selected').val() == 'default')
                $body.addClass('header-default');
            else
                $body.removeClass('header-default');
        });
    };
    var setSideBarStyle = function() {
        $('select[name="sidebar"]').change(function() {
            if ($('select[name="sidebar"] option:selected').val() == 'fixed') {
                sideLeft.removeClass('slide-default');

                if ($body.hasClass("isMobile") == false) {
                    mainNavigation.perfectScrollbar({
                        wheelSpeed: 50,
                        minScrollbarLength: 20,
                        suppressScrollX: true
                    });

                }
            } else {
                sideLeft.addClass('slide-default');
                mainNavigation.perfectScrollbar("destroy");
            }
        });
    };
    var setFooterStyle = function() {
        $('select[name="footer"]').change(function() {
            if ($('select[name="footer"] option:selected').val() == 'fixed') {
                $body.addClass('footer-fixed');
            } else {
                $body.removeClass('footer-fixed');
            }
        });
    };
    //function to activate ColorPalette
    var runColorPalette = function() {
        if ($('.colorpalette').length) {
            $('.colorpalette').colorPalette().on('selectColor', function(e) {
                $(this).closest('ul').prev('a').children('i').css('background-color', e.color).end().closest('div').prev('input').val(e.color);
                runActivateLess();
            });
        };
    };

    // Window Resize Function
    var runWIndowResize = function(func, threshold, execAsap) {
        //wait until the user is done resizing the window, then execute
        $(window).espressoResize(function() {
            runElementsPosition();
            setPortfolioPanel();
            runRefreshSliders();
        });
    };
    //function to select all checkboxes in a Table
    var runCheckAll = function() {
        $('input[type="checkbox"].selectall').on('ifChecked', function(event) {
            $(this).closest("table").find(".foocheck").iCheck('check');
        }).on('ifUnchecked', function(event) {
            $(this).closest("table").find(".foocheck").iCheck('uncheck');
        });
    };
    // function to activate Responsive Portfolio Panel
    var setPortfolioPanel = function() {
        $(".portfolio-grid .item").each(function() {
            var portfolioImageW = $(this).closest(".portfolio-grid").outerWidth();
            var portfolioMaxHeight = parseInt($(this).closest(".portfolio-grid").css("max-height"));
            if (isNaN(portfolioMaxHeight) == false) {
                var portfolioImage = $(this).find("img");
                var img = new Image();
                img.onload = function() {
                    var thisWidth = portfolioImage.width();
                    var thisHeight = portfolioImage.height();
                    var thisNewWidth = portfolioImageW;
                    var thisNewHeight = thisNewWidth * thisHeight / thisWidth;
                    if (thisNewHeight < portfolioMaxHeight) {
                        thisNewHeight = portfolioMaxHeight;
                        thisNewWidth = thisNewHeight * thisWidth / thisHeight;
                        if (thisNewWidth >= portfolioImageW) {
                            portfolioImage.velocity({
                                width: thisNewWidth,
                                height: thisNewHeight,
                                left: -(thisNewWidth - portfolioImageW) / 2,
                                top: 0
                            });
                        } else {
                            thisNewWidth = portfolioImageW;
                            thisNewHeight = thisNewWidth * thisHeight / thisWidth;

                            portfolioImage.velocity({
                                width: thisNewWidth,
                                height: thisNewHeight,
                                top: -(thisNewHeight - portfolioMaxHeight) / 2,
                                left: 0
                            });
                        };
                    } else {

                        thisNewWidth = portfolioImageW;
                        thisNewHeight = thisNewWidth * thisHeight / thisWidth;

                        portfolioImage.velocity({
                            width: thisNewWidth,
                            height: thisNewHeight,
                            top: -(thisNewHeight - portfolioMaxHeight) / 2,
                            left: 0
                        });
                    }

                };
                img.src = portfolioImage.attr("src");
            }
        });
        var owlPortfolio = $(".panel-portfolio .e-slider").data('owlCarousel');
        $(".panel-portfolio .owl-next").off().on("click", function(e) {
            owlPortfolio.next();
            e.preventDefault();
        });
        $(".panel-portfolio .owl-prev").off().on("click", function(e) {
            owlPortfolio.prev();
            e.preventDefault();
        });
    };

    //function to save user settings
    var runSaveSetting = function() {
        $('.save_style').on('click', function() {
            var espressoSetting = new Object;
            if ($body.hasClass('layout-boxed')) {
                espressoSetting.layoutBoxed = true;
                $("body[class]").filter(function() {
                    var classNames = this.className.split(/\s+/);
                    for (var i = 0; i < classNames.length; ++i) {
                        if (classNames[i].substr(0, 9) === "bg_style_") {
                            espressoSetting.bgStyle = classNames[i];
                        }
                    }

                });
            } else {
                espressoSetting.layoutBoxed = false;
            };
            if ($body.hasClass('header-default')) {
                espressoSetting.headerDefault = true;
            } else {
                espressoSetting.headerDefault = false;
            };
            if ($body.hasClass('footer-fixed')) {
                espressoSetting.footerDefault = false;
            } else {
                espressoSetting.footerDefault = true;
            };
            if (sideLeft.hasClass('slide-default')) {
                espressoSetting.slideDefault = true;
            } else {
                espressoSetting.slideDefault = false;
            };
            espressoSetting.skinClass = $('#skin_color').attr('href');

            $.cookie("espresso-setting", JSON.stringify(espressoSetting));

            var el = $('#style_selector_container');
            el.block({
                overlayCSS: {
                    backgroundColor: '#000'
                },
                message: '<i class="fa fa-spinner fa-spin"></i>',
                css: {
                    border: 'none',
                    color: '#fff',
                    background: 'none'
                }
            });
            window.setTimeout(function() {
                el.unblock();
            }, 1000);
        });
    };
    //function to load user settings
    var runCustomSetting = function() {
        if ($.cookie) {
            if ($.cookie("espresso-setting")) {
                var loadSetting = jQuery.parseJSON($.cookie("espresso-setting"));
                if (loadSetting.layoutBoxed) {

                    $body.addClass('layout-boxed');
                    $('#style_selector select[name="layout"]').find('option[value="boxed"]').attr('selected', 'true');
                };
                if (loadSetting.headerDefault) {
                    $body.addClass('header-default');
                    $('#style_selector select[name="header"]').find('option[value="default"]').attr('selected', 'true');
                };
                if (!loadSetting.footerDefault) {
                    $body.addClass('footer-fixed');
                    $('#style_selector select[name="footer"]').find('option[value="fixed"]').attr('selected', 'true');
                };
                if (loadSetting.slideDefault) {
                    sideLeft.addClass('slide-default');
                    $('#style_selector select[name="sidebar"]').find('option[value="default"]').attr('selected', 'true');
                };
                if ($('#style_selector').length) {
                    $('#skin_color').attr('href', loadSetting.skinClass);

                };
                $body.addClass(loadSetting.bgStyle);
            } else {
                runDefaultSetting();
            };
        }
    };

    //function to clear user settings
    var runClearSetting = function() {
        $('.clear_style').on('click', function() {
            $.removeCookie("espresso-setting");
            $body.removeClass("layout-boxed header-default footer-fixed");
            sideLeft.removeClass('slide-default');
            $body[0].className = $body[0].className.replace(/\bbg_style_.*?\b/g, '');
            if ($('#skin_color').attr("rel") == "stylesheet/less") {
                $('#skin_color').next('style').remove();
                $('#skin_color').attr("rel", "stylesheet");

            }

            $('.icons-color img').first().trigger('click');
            runDefaultSetting();
        });
    };
    //function to restore user settings
    var runDefaultSetting = function() {
        $('#style_selector select[name="layout"]').val('default');
        $('#style_selector select[name="header"]').val('fixed');
        $('#style_selector select[name="footer"]').val('default');
        $('#style_selector select[name="sidebar"]').val('fixed');
        $('.boxed-patterns img').removeClass('active');
    };
    //function to set the User Staus (Online/Offline)
    var runStatusButton = function() {
        $(".btn.status").on("click", function(e) {
            if ($(this).hasClass("offline")) {
                $(this).removeClass("offline").find("span").text("Online");

            } else {
                $(this).addClass("offline").find("span").text("Offline");
            }
            e.preventDefault();
        });
    };
    //function to animate Progressbar when appear
    var runAnimateProgressbar = function() {
        var progressBarDefaultsOptions = {
            transition_delay: 0
        };
        $('.progress .animate-progress-bar').each(function() {
            var configProgressBar = $.extend({}, progressBarDefaultsOptions, $(this).data("plugin-options"));
            if ($(this).is(':appeared') || isMobile) {
                $(this).progressbar(configProgressBar);
            } else {
                $(this).appear();
                $(this).on("appear", function(event, $all_appeared_elements) {
                    $(this).progressbar(configProgressBar);
                });
            }
        });
    };
    var runMsViewport = function() {
        if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
            var msViewportStyle = document.createElement("style");
            msViewportStyle.appendChild(document.createTextNode("@-ms-viewport{width:auto!important}"));
            document.getElementsByTagName("head")[0].appendChild(msViewportStyle);
        }
    };
    return {
        //main function to initiate template pages
        init: function() {
            runWIndowResize();
            runInit();
            runQuickChat();
            runToggleSideBars();
            runStyleSelector();
            runElementsPosition();
            runToDoAction();
            runNavigationMenu();
            runGoTop();
            setSearchMenu();
            runModuleTools();
            runDropdownEnduring();
            runTooltips();
            runESlider();
            runPopovers();
            runPanelScroll();
            runAnimatedElements();
            runShowTab();
            runCustomCheck();
            runColorPalette();
            runSaveSetting();
            runCustomSetting();
            runStatusButton();
            runCheckAll();
            runClearSetting();
            runClosedBarButton();
            runAnimateProgressbar();
            runSelecticker();
            setPortfolioPanel();
            runSideBarToggle();
            runMsViewport();
            runTimeStamp();
            documentEvents();
        }
    };
}();

$(window).bind("load", function () {
    // Remove splash screen after load
    $('.splash').css('display', 'none')
});
//# sourceMappingURL=login.js.map
