# Model Feedback
class Feedback < ActiveRecord::Base
  self.table_name = 'feedbacks'

  belongs_to :user
  validates_presence_of :message
end