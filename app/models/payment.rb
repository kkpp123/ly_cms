# Payment Model
class Payment < ActiveRecord::Base
  self.table_name = 'payments'

  belongs_to :user
end
