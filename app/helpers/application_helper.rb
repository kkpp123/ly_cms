module ApplicationHelper
  #【引数】 提示框样式选择
  #【返回】
  #【注意】
  #【著作】lx by 2018-6-14
  def switch_key(key)
    case key.to_s
    when 'alert'
      'warning'
    when 'notice'
      'success'
    else
      key
    end
  end
end
